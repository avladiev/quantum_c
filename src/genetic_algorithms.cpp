#include "genetic_algorithms.hpp"
#include "random.hpp"

MateImpl::MateImpl(float _pMate, float _factor) :
		pMate(_pMate), factor(_factor) {
}

void setOldFitness(SchemePtr scheme, double oldFiness) {
	if (!scheme->isAppliedGAOperator()) {
		scheme->setOldFitness(oldFiness);
	}
}
std::pair<SchemePtr, SchemePtr> makeCopyScheme(const SchemePtr& parent1,
		const SchemePtr& parent2) {
	SchemePtr chaild1(new Scheme(*parent1));

	setOldFitness(chaild1, parent1->getFitness());
	SchemePtr chaild2(new Scheme(*parent2));
	setOldFitness(chaild2, parent2->getFitness());
	return std::make_pair(chaild1, chaild2);
}

std::pair<SchemePtr, SchemePtr> MateImpl::mate(const SchemePtr& parent1,
		const SchemePtr& parent2) const {

	if (Random::nextRandomDouble() < pMate) {
		std::vector<Layer> & layersP1 = parent1->getLayers();
		std::vector<Layer> & layersP2 = parent2->getLayers();
		int minSize = std::min(layersP1.size(), layersP2.size());
		if (minSize == 1) {
			return makeCopyScheme(parent1, parent2);
		}

		SchemePtr chaild1(new Scheme());
		SchemePtr chaild2(new Scheme());
		std::vector<Layer> & layersChild1 = chaild1->getLayers();
		std::vector<Layer> & layersChild2 = chaild2->getLayers();

		int pos = Random::nextRandomInt(minSize - 1) + 1;

		for (int i = 0; i < pos; ++i) {
			layersChild1.push_back(layersP2.at(i));
			layersChild2.push_back(layersP1.at(i));
		}

		for (int i = pos; i < layersP1.size(); ++i) {
			layersChild1.push_back(layersP1.at(i));
		}
		for (int i = pos; i < layersP2.size(); ++i) {
			layersChild2.push_back(layersP2.at(i));
		}

		double maxFitness = std::max(parent1->getFitness(),
				parent2->getFitness());
		setOldFitness(chaild1, maxFitness);
		setOldFitness(chaild2, maxFitness);
		chaild1->setMate();
		chaild2->setMate();
		return std::make_pair(chaild1, chaild2);
	} else {
		return makeCopyScheme(parent1, parent2);
	}

}

void MateImpl::mate(std::vector<SchemePtr>& in,
		std::vector<SchemePtr>& out) const {
	for (int i = 0; i < in.size(); ++i) {
		SchemePtr & p1 = in[i];
		for (int j = 0; j < factor; ++j) {
			int i2 = Random::nextRandomInt(in.size());
			SchemePtr & p2 = in[i2];
			std::pair<SchemePtr, SchemePtr> children = mate(p1, p2);
			out.push_back(children.first);
			out.push_back(children.second);
		}
	}
}
std::string MateImpl::toString() const {
	return " MateImpl";
}

MateImpl::~MateImpl() {

}

void ElitistSelection::selection(std::vector<SchemePtr> & in,
		std::vector<SchemePtr> & out, int sizePopulation) const {
	int size = std::min(sizePopulation, (int) in.size());
	for (int i = 0; i < size; ++i) {
		out.push_back(in[i]);
	}
}

std::string ElitistSelection::toString() const {
	return "SimpleSelection";
}
ElitistSelection::~ElitistSelection() {
}

void TourneySelection::selection(std::vector<SchemePtr> & in,
		std::vector<SchemePtr> & out, int sizePopulation) const {
	int requiredSize = std::min(sizePopulation, (int) in.size());
	int curSize = in.size();

	int sizeGroup = curSize / requiredSize;

	for (int i = 0, j = 0; i < in.size() && j < requiredSize; ++i) {
		if (i % sizeGroup == 0) {
			out.push_back(in[i]);
			++j;
		}
	}

}
std::string TourneySelection::toString() const {
	return "TourneySelection";
}
TourneySelection::~TourneySelection() {

}

ElitistTourneySelection::ElitistTourneySelection(double _elitis) :
		elitis(_elitis) {
}

void ElitistTourneySelection::selection(std::vector<SchemePtr> & in,
		std::vector<SchemePtr> & out, int sizePopulation) const {
	int requiredSize = std::min(sizePopulation, (int) in.size());
	int curSize = in.size();

	int elitistSize = requiredSize * elitis;

	for (int i = 0; i < elitistSize; ++i) {
		out.push_back(in[i]);
	}

	requiredSize -= elitistSize;

	if (requiredSize <= 0) {
		return;
	}

	int sizeGroup = (curSize - elitistSize) / requiredSize;

	if (sizeGroup <= 0) {
		return;
	}

	for (int i = elitistSize, j = 0; i < in.size() && j < requiredSize; ++i) {
		if (i % sizeGroup == 0) {
			out.push_back(in[i]);
			++j;
		}
	}

}

std::string ElitistTourneySelection::toString() const {
	return std::string("ElitistTourneySelection elist = ").append(
			Util::doubleToString(elitis));
}
ElitistTourneySelection::~ElitistTourneySelection() {

}

void countFitnessByNorm(const SchemePtr& scheme,
		double (*matrixNorm)(const IMatrix & matrix)) {
	IMatrix& m = (scheme->countSchemeMatrix());
	MatrixImpl sub(m.getNumberRows(), m.getNumberColumns());
	IMatrix::subtraction(m, Locator::instance().getTargetMatrix(), sub);
	double fitness = matrixNorm(sub);
	scheme->setFitness(fitness);
}

void FitnessByL2Norm::countFitness(const SchemePtr& ptr) const {
	countFitnessByNorm(ptr, IMatrix::countSquareL2Norm);
}

std::string FitnessByL2Norm::toString() const {
	return "FitnessByL2Norm";
}

FitnessByL2Norm::~FitnessByL2Norm() {

}

void FitnessByOperatorL1Norm::countFitness(const SchemePtr & scheme) const {
	LOG(DEBUG, "fix2");
	countFitnessByNorm(scheme, IMatrix::countOperatorL1Norm);
}

FitnessByOperatorL1Norm::~FitnessByOperatorL1Norm() {

}

void FitnessByOperatorL2Norm::countFitness(const SchemePtr& scheme) const {
	countFitnessByNorm(scheme, IMatrix::countSquareL2Norm);
}

FitnessByOperatorL2Norm::~FitnessByOperatorL2Norm() {
}

void FitnessByInfNorm::countFitness(const SchemePtr& scheme) const {
	countFitnessByNorm(scheme, IMatrix::countSquareInfNorm);
}

FitnessByInfNorm::~FitnessByInfNorm() {
}

MutationImpl::MutationImpl(float _pMutation, float _pInversion) :
		pMutation(_pMutation), pInversion(_pInversion) {

}
void MutationImpl::mutate(const SchemePtr& scheme) const {
	std::vector<Layer> & layers = scheme->getLayers();

	int randomNum = Random::nextRandomInt(3);
	if (randomNum == 0) {
		int randomIndex = Random::nextRandomInt(layers.size());
		layers[randomIndex] = Layer::makeRandomLayer();
	} else if (randomNum == 1) {
		if (layers.size() > 1) {
			layers.pop_back();
		}
	} else {
		Layer randomLayer = Layer::makeRandomLayer();
		layers.push_back(randomLayer);
	}
}

void MutationImpl::inversion(const SchemePtr& scheme) const {
	std::vector<Layer> & layers = scheme->getLayers();
	int rand1 = Random::nextRandomInt(layers.size());
	int rand2 = Random::nextRandomInt(layers.size());
	int a = std::min(rand1, rand2);
	int b = std::max(rand1, rand2) + 1;
	std::reverse(layers.begin() + a, layers.begin() + b);
}

void MutationImpl::mutate(std::vector<SchemePtr>& in) const {
	for (int i = 0; i < in.size(); ++i) {
		if (Random::nextRandomDouble() < pMutation) {
			mutate(in[i]);
			setOldFitness(in[i], in[i]->getOldFitness());
			in[i]->setMutate();
		}
		if (Random::nextRandomDouble() < pInversion) {
			inversion(in[i]);
			setOldFitness(in[i], in[i]->getOldFitness());
			in[i]->setInversion();
		}
	}
}

std::string MutationImpl::toString() const {
	return "MutationImpl";
}
MutationImpl::~MutationImpl() {
}

