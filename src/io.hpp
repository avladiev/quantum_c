#ifndef IO_HPP_
#define IO_HPP_
#include <string>
#include "hdf5.h"
#include "util.hpp"
#include "matrix.hpp"
#include <vector>
#include <cmath>
#include <iostream>
#include <vector>

class IOModule {
private:
	static void writeMatrix(hid_t fileId, const IMatrix & matrix,
			const std::string & realPathName, const std::string & imagPathName);
	static void writeFitness(hid_t fileId, double fitness);
	static void writeSchemeStr(hid_t fileId, const std::string & schemeStr);
public:

	static void writeTargetMatrix(const std::string& fileName,
			const IMatrix & matrix);
	static void writeBaseMatrices(const std::string & fileName,
			std::vector<IMatrix*>* matrices);
	static void write(const std::string& fileName, double fitness,
			const IMatrix & matrix, const std::string & scheme);
	static std::vector<IMatrix*>* readBaseMatrices(
			const std::string & baseMatrixFileName);
	static IMatrix* readTargetMatrix(const std::string & targetMatrixFileName);

};

#endif
