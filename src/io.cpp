#include "io.hpp"

/*InputData* IOModule::read(const std::string & fileName) {
 Open an existing file.
 hid_t inputFileId = H5Fopen(fileName.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
 Open an  dataset  name is  targetMatrix
 hid_t dataSetId = H5Dopen2(inputFileId, "targetMatrix", H5P_DEFAULT);
 hid_t dataSpaceId = H5Dget_space(dataSetId);
 hsize_t dims[2];

 H5Sget_simple_extent_dims(dataSpaceId, dims, 0);
 double* targetMatrixData = new double[dims[0] * dims[1]];
 herr_t status = H5Dread(dataSetId, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL,
 H5P_DEFAULT, targetMatrixData);
 IMatrix* targetMatrix = (new MatrixImpl(dims[0], dims[1], targetMatrixData));

 status = H5Dclose(dataSetId);

 //Open an  dataset name  is  numberBaseMatrix

 int numberBaseMatrixRaw[1][1];
 dataSetId = H5Dopen(inputFileId, "numberBaseMatrix", H5P_DEFAULT);
 H5Dread(dataSetId, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
 numberBaseMatrixRaw);
 int numberBaseMatrix = numberBaseMatrixRaw[0][0];
 H5Dclose(dataSetId);

 //read base matrix
 std::vector<IMatrix*> *baseMatrix = new std::vector<IMatrix*>;
 for (hsize_t i = 0; i < numberBaseMatrix; ++i) {
 std::string matrixName = "baseMatrix";
 matrixName.append(Util::intToString(i));
 dataSetId = H5Dopen2(inputFileId, matrixName.c_str(), H5P_DEFAULT);
 dataSpaceId = H5Dget_space(dataSetId);
 H5Sget_simple_extent_dims(dataSpaceId, dims, 0);
 double* matrixData = new double[dims[0] * dims[1]];
 H5Dread(dataSetId, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
 matrixData);
 IMatrix* matrix = (new MatrixImpl(dims[0], dims[1], matrixData));
 baseMatrix->push_back(matrix);
 }

 H5Dclose(dataSetId);
 H5Fclose(inputFileId);
 return new InputData(targetMatrix, baseMatrix);
 }*/

void fullRealPart(IMatrix * matrix, double * data) {
	int numberRows = matrix->getNumberRows();
	for (int i = 0; i < matrix->getNumberRows(); ++i) {
		for (int j = 0; j < matrix->getNumberColumns(); ++j) {
			Cell cell = matrix->get(i, j);
			cell.real(data[i * numberRows + j]);
			matrix->set(i, j, cell);
		}
	}
}

void fullImagPart(IMatrix * matrix, double * data) {
	int numberRows = matrix->getNumberRows();
	for (int i = 0; i < matrix->getNumberRows(); ++i) {
		for (int j = 0; j < matrix->getNumberColumns(); ++j) {
			Cell cell = matrix->get(i, j);
			cell.imag(data[i * numberRows + j]);
			matrix->set(i, j, cell);
		}
	}
}

IMatrix* IOModule::readTargetMatrix(const std::string & targetMatrixFileName) {
	/* Open antargetMatrixFile */
	hid_t inputFileId = H5Fopen(targetMatrixFileName.c_str(), H5F_ACC_RDWR,
			H5P_DEFAULT);
	/* Open an  dataset  name is  real */
	hid_t dataSetId = H5Dopen2(inputFileId, "real", H5P_DEFAULT);
	hid_t dataSpaceId = H5Dget_space(dataSetId);
	hsize_t dims[2];

	H5Sget_simple_extent_dims(dataSpaceId, dims, 0);
	double* targetMatrixData = new double[dims[0] * dims[1]];
	herr_t status = H5Dread(dataSetId, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL,
			H5P_DEFAULT, targetMatrixData);
	IMatrix* targetMatrix = new MatrixImpl(dims[0], dims[1]);
	fullRealPart(targetMatrix, targetMatrixData);
	status = H5Dclose(dataSetId);
	/* Open an  dataset  name is  imag */
	dataSetId = H5Dopen2(inputFileId, "imag", H5P_DEFAULT);
	status = H5Dread(dataSetId, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
			targetMatrixData);
	fullImagPart(targetMatrix, targetMatrixData);
	status = H5Dclose(dataSetId);
	H5Fclose(inputFileId);
	delete targetMatrixData;
	return targetMatrix;
}

std::vector<IMatrix*>* IOModule::readBaseMatrices(
		const std::string & baseMatrixFileName) {

	hid_t inputFileId = H5Fopen(baseMatrixFileName.c_str(), H5F_ACC_RDWR,
			H5P_DEFAULT);

	//Open an  dataset name  is  numberBaseMatrix
	int numberBaseMatrixRaw[1][1];
	hid_t dataSetId = H5Dopen(inputFileId, "numberBaseMatrix", H5P_DEFAULT);
	H5Dread(dataSetId, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT,
			numberBaseMatrixRaw);
	int numberBaseMatrix = numberBaseMatrixRaw[0][0];
	H5Dclose(dataSetId);

	hsize_t dims[2];
	//read base matrix
	std::vector<IMatrix*> *baseMatrix = new std::vector<IMatrix*>;
	for (hsize_t i = 0; i < numberBaseMatrix; ++i) {

		std::string matrixName = "baseMatrixReal";
		matrixName.append(Util::intToString(i));
		dataSetId = H5Dopen2(inputFileId, matrixName.c_str(), H5P_DEFAULT);
		hid_t dataSpaceId = H5Dget_space(dataSetId);
		H5Sget_simple_extent_dims(dataSpaceId, dims, 0);
		double* matrixData = new double[dims[0] * dims[1]];
		H5Dread(dataSetId, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
				matrixData);
		H5Dclose(dataSetId);
		IMatrix* matrix = (new MatrixImpl(dims[0], dims[1]));
		fullRealPart(matrix, matrixData);

		matrixName = "baseMatrixImag";
		matrixName.append(Util::intToString(i));
		dataSetId = H5Dopen2(inputFileId, matrixName.c_str(), H5P_DEFAULT);
		H5Dread(dataSetId, H5T_IEEE_F64LE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
				matrixData);
		H5Dclose(dataSetId);
		fullImagPart(matrix, matrixData);

		baseMatrix->push_back(matrix);
		delete matrixData;
	}

	H5Fclose(inputFileId);
	return baseMatrix;

}

void fullRawDataRealPath(double * rawData, const IMatrix& matrix) {
	int numberRows = matrix.getNumberRows();
	for (int i = 0; i < matrix.getNumberRows(); ++i) {
		for (int j = 0; j < matrix.getNumberColumns(); ++j) {
			rawData[i * numberRows + j] = matrix.get(i, j).real();
		}
	}
}

void fullRawDataImagPath(double * rawData, const IMatrix& matrix) {
	int numberRows = matrix.getNumberRows();
	for (int i = 0; i < matrix.getNumberRows(); ++i) {
		for (int j = 0; j < matrix.getNumberColumns(); ++j) {
			rawData[i * numberRows + j] = matrix.get(i, j).imag();
		}
	}
}

/*void IOModule::write(const std::string & fileName,
 const OutputData& outputData) {
 IMatrix const * matrix = &outputData.matrix;
 hsize_t dims[2];
 dims[0] = matrix->getNumberRows();
 dims[1] = matrix->getNumberColumns();
 //create file
 hid_t fileId = H5Fcreate(fileName.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT,
 H5P_DEFAULT);
 double* rawData = new double[dims[0] * dims[1]];
 //save real path
 fullRawDataRealPath(rawData, *matrix);

 hid_t fileSpaceId = H5Screate_simple(2, dims, 0);
 hid_t dataSetId = H5Dcreate(fileId, "real", H5T_IEEE_F64LE
 , fileSpaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
 H5Dwrite(dataSetId, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
 rawData);
 H5Dclose(dataSetId);
 H5Sclose(fileSpaceId);
 //save imag path
 fullRawDataImagPath(rawData, *matrix);
 fileSpaceId = H5Screate_simple(2, dims, 0);
 dataSetId = H5Dcreate(fileId, "imag", H5T_IEEE_F64LE
 , fileSpaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
 H5Dwrite(dataSetId, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
 rawData);
 delete[] rawData;
 H5Dclose(dataSetId);
 H5Sclose(fileSpaceId);
 //save fitness
 dims[0] = 1;
 dims[1] = 1;
 fileSpaceId = H5Screate_simple(2, dims, 0);

 dataSetId = H5Dcreate(fileId, "fitness", H5T_IEEE_F64LE
 , fileSpaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
 double tmp = outputData.fitness;
 H5Dwrite(dataSetId, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, &tmp);
 H5Dclose(dataSetId);
 H5Sclose(fileSpaceId);

 //save scheme


 * Create file and memory datatypes.  For this example we will save
 * the strings as FORTRAN strings, therefore they do not need space
 * for the null terminator in the file.

 hid_t filetype = H5Tcopy(H5T_FORTRAN_S1);
 const int sizeBuf = outputData.schemaInStr.size();
 hid_t status = H5Tset_size(filetype, sizeBuf - 1);
 hid_t memtype = H5Tcopy(H5T_C_S1);
 status = H5Tset_size(memtype, sizeBuf);


 * Create dataspace.  Setting maximum size to NULL sets the maximum
 * size to be the current size.

 hsize_t dim = 1;
 hid_t space = H5Screate_simple(1, &dim, 0);


 * Create the dataset and write the string data to it.

 hid_t dset = H5Dcreate(fileId, "schemeStr", filetype, space, H5P_DEFAULT,
 H5P_DEFAULT, H5P_DEFAULT);

 const char * data = outputData.schemaInStr.c_str();
 status = H5Dwrite(dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);

 * Close and release resources.

 status = H5Dclose(dset);
 status = H5Sclose(space);
 status = H5Tclose(filetype);
 status = H5Tclose(memtype);

 H5Fclose(fileId);
 }*/

void IOModule::writeFitness(hid_t fileId, double fitness) {
	hsize_t dims[2];
	dims[0] = 1;
	dims[1] = 1;
	hsize_t fileSpaceId = H5Screate_simple(2, dims, 0);
	hsize_t dataSetId = H5Dcreate(fileId, "fitness", H5T_IEEE_F64LE
	, fileSpaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	H5Dwrite(dataSetId, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
			&fitness);
	H5Dclose(dataSetId);
	H5Sclose(fileSpaceId);
}

void IOModule::writeSchemeStr(hid_t fileId, const std::string & schemeStr) {
	//save scheme
	/*
	 * Create file and memory datatypes.  For this example we will save
	 * the strings as FORTRAN strings, therefore they do not need space
	 * for the null terminator in the file.
	 */
	hid_t filetype = H5Tcopy(H5T_FORTRAN_S1);
	const int sizeBuf = schemeStr.size();
	hid_t status = H5Tset_size(filetype, sizeBuf - 1);
	hid_t memtype = H5Tcopy(H5T_C_S1);
	status = H5Tset_size(memtype, sizeBuf);

	/*
	 * Create dataspace.  Setting maximum size to NULL sets the maximum
	 * size to be the current size.
	 */
	hsize_t dim = 1;
	hid_t space = H5Screate_simple(1, &dim, 0);

	/*
	 * Create the dataset and write the string data to it.
	 */
	hid_t dset = H5Dcreate(fileId, "schemeStr", filetype, space, H5P_DEFAULT,
			H5P_DEFAULT, H5P_DEFAULT);

	const char * data = schemeStr.c_str();
	status = H5Dwrite(dset, memtype, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
	/*
	 * Close and release resources.
	 */
	status = H5Dclose(dset);
	status = H5Sclose(space);
	status = H5Tclose(filetype);
	status = H5Tclose(memtype);
}

void IOModule::write(const std::string& fileName, double fitness,
		const IMatrix & matrix, const std::string & scheme) {
	//create file
	hid_t fileId = H5Fcreate(fileName.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT,
			H5P_DEFAULT);
	writeMatrix(fileId, matrix, "real", "imag");
	writeFitness(fileId, fitness);
	writeSchemeStr(fileId, scheme);
	H5Fclose(fileId);
}

void IOModule::writeMatrix(hid_t fileId, const IMatrix & matrix,
		const std::string & realPathName, const std::string & imagPathName) {
	hsize_t dims[2];
	dims[0] = matrix.getNumberRows();
	dims[1] = matrix.getNumberColumns();
	double* rawData = new double[dims[0] * dims[1]];

	//save real path
	fullRawDataRealPath(rawData, matrix);
	hid_t fileSpaceId = H5Screate_simple(2, dims, 0);
	hid_t dataSetId = H5Dcreate(fileId, realPathName.c_str(), H5T_IEEE_F64LE
	, fileSpaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	H5Dwrite(dataSetId, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
			rawData);
	H5Dclose(dataSetId);
	H5Sclose(fileSpaceId);

	//save imag path
	fullRawDataImagPath(rawData, matrix);
	fileSpaceId = H5Screate_simple(2, dims, 0);
	dataSetId = H5Dcreate(fileId, imagPathName.c_str(), H5T_IEEE_F64LE
	, fileSpaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
	H5Dwrite(dataSetId, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT,
			rawData);
	H5Dclose(dataSetId);
	H5Sclose(fileSpaceId);
	delete[] rawData;
}

void IOModule::writeTargetMatrix(const std::string& fileName,
		const IMatrix & matrix) {
	//create file
	hid_t fileId = H5Fcreate(fileName.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT,
			H5P_DEFAULT);
	writeMatrix(fileId, matrix, "real", "imag");
	H5Fclose(fileId);
}

void IOModule::writeBaseMatrices(const std::string & fileName,
		std::vector<IMatrix*>* matrices) {
	//create file
	hid_t fileId = H5Fcreate(fileName.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT,
			H5P_DEFAULT);

	for (int i = 0; i < matrices->size(); ++i) {
		IMatrix * matrix = matrices->at(i);
		std::string realName = "baseMatrixReal";
		std::string imagName = "baseMatrixImag";
		realName.append(Util::intToString(i));
		imagName.append(Util::intToString(i));
		writeMatrix(fileId, *matrix, realName, imagName);
	}

	int size = matrices->size();
	hsize_t dims[2];
	dims[0] = 1;
	dims[1] = 1;
	hid_t fileSpaceId = H5Screate_simple(2, dims, 0);
	hid_t dataSetId = H5Dcreate(fileId, "numberBaseMatrix", H5T_STD_I32LE
	, fileSpaceId, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	H5Dwrite(dataSetId, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, &size);
	H5Dclose(dataSetId);
	H5Fclose(fileId);
}
