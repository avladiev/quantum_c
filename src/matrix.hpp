#ifndef MATRIX2_HPP_
#define MATRIX2_HPP_

#include "exeptions.hpp"
#include <sstream>
#include <string>
#include <bitset>
#include "iostream"
#include <complex>
#include "Logger.h"
#include "random.hpp"

typedef std::complex<double> Cell;
class IMatrix {
public:
	virtual Cell get(int i, int j) const = 0;
	virtual void set(int i, int j, const Cell & val) = 0;
	virtual int getNumberRows() const = 0;
	virtual int getNumberColumns() const= 0;

	static void multiplication(const IMatrix & m1, const IMatrix &m2,
			IMatrix & out);
	static void subtraction(const IMatrix & m1, const IMatrix &m2,
			IMatrix & out);
	/**
	 * считает квадрат эвклидовой нормы
	 */
	static double countSquareL2Norm(const IMatrix & matrix);
	static double countL1Norm(const IMatrix & matrix);
	/**
	 * считает квадрат операторной нормы по L2
	 */
	static double countSquareOperatorL2Norm(const IMatrix & matrix);

	/**
	 * считает операторную норму по L1
	 */
	static double countOperatorL1Norm(const IMatrix & matrix);
	/**
	 * считает операторную норму
	 */
	static double countOperatorNorm(const IMatrix & matrix,
			double (*vectorNorm)(const IMatrix & matrix));

	/**
	 * считает квадрат нормы maxij[aij]
	 */
	static double countSquareInfNorm(const IMatrix & matrix);

	static void copy(const IMatrix& src, IMatrix& decs);

	virtual ~IMatrix();
};
std::ostream & operator<<(std::ostream &out, const IMatrix & m);
class MatrixImpl: public IMatrix {
private:
	Cell* data;
	const int numberRows;
	const int numberColumns;
	MatrixImpl &operator =(const MatrixImpl &);
	void checkBorders(int i, int j) const;
public:

	MatrixImpl(int numberRows_, int numberColumns_);

	MatrixImpl(std::pair<int, int> size);

	MatrixImpl(int numberRows_, int numberColumns_, Cell* data_);

	MatrixImpl(int size);

	MatrixImpl(IMatrix &matrix);

	Cell get(int i, int j) const;
	void set(int i, int j, const Cell & val);
	int getNumberRows() const;
	int getNumberColumns() const;
	~MatrixImpl();

};

#endif
