#include "locator.hpp"

Locator::Locator() {

}

Locator::~Locator() {
	for (int i = 0; i < baseMatrix->size(); ++i) {
		delete baseMatrix->operator [](i);
	}
	delete baseMatrix;
	delete targetMatrix;
}

void Locator::init(std::vector<IMatrix*>* bM, IMatrix* targetMatrix) {
	baseMatrix = bM;
	this->targetMatrix = targetMatrix;
	numberQubitInScheme = Util::countNumberQubit(targetMatrix->getNumberRows());
}

int Locator::getNumberBaseMatrix() {
	return baseMatrix->size();
}

const IMatrix& Locator::getBaseMatrix(int desc) {
	return *baseMatrix->at(desc);
}

const IMatrix& Locator::getTargetMatrix() {
	return *targetMatrix;
}

int Locator::getNumberQubitInScheme() {
	return numberQubitInScheme;
}

std::pair<int, int> Locator::getSizeTargetMatrix() {
	return std::make_pair(targetMatrix->getNumberRows(),
			targetMatrix->getNumberRows());
}

template<>
Locator* Singleton<Locator>::myInstance = 0;

