


#include <iostream>
#include "io.hpp"
#include "model2.hpp"
#include "Logger.h"
#include "locator.hpp"
#include <unistd.h>
#include <cassert>
#include "random.hpp"

int main(int argc, char** argv) {

	Random::init();
	LOG_CONF("log", Logger::file_off|Logger::screen_on, DEBUG, DEBUG, 0);

	LOG(INFO, "generator is running");

	std::string baseMatrixFileName;
	std::string targetMatrixFileName;
	targetMatrixFileName = "targetMatrix_.h5";

	std::string targetMatrixFileName2 = "targetMatrix_2.h5";
	std::string targetMatrixFileName3 = "targetMatrix_3.h5";

	std::cout << targetMatrixFileName << "\n";

	std::vector<IMatrix*>* baseMatrix = new std::vector<IMatrix*>();

	baseMatrix->push_back( Gate::getHGate());
	baseMatrix->push_back(Gate::getXGate());
	//baseMatrix->push_back(Gate::getCNotGate());
	baseMatrix->push_back(Gate::getZGate());


	double eps = 0.0001;
	for (double alpha = 0; alpha < 2 * M_PI + eps; alpha += M_PI / 6) {
		baseMatrix->push_back(Gate::getRxGate(alpha));
		baseMatrix->push_back(Gate::getRyGate(alpha));
		baseMatrix->push_back(Gate::getRzGate(alpha));
	}

	int numberQubit = 3;
	int sizeMatrix = 1 << (numberQubit);
	int numberLayers = 5;
	MatrixImpl matrix(sizeMatrix);
	Locator::instance().init(baseMatrix, &matrix);

	SchemePtr scheme = Scheme::makeRandomScheme(numberLayers);

	LOG(INFO, "make scheme \n"<<*scheme);
	scheme->countSchemeMatrix();
	LOG(INFO, "\n"<<scheme->getSchemeMatrix());
	IOModule::write(targetMatrixFileName,0,
			scheme->getSchemeMatrix(),scheme->toString());


	numberQubit = 6;
	sizeMatrix = 1 << (numberQubit);
	MatrixImpl matrix2(sizeMatrix);
	Locator::instance().init(baseMatrix, &matrix2);
	SchemePtr newScheme(new Scheme());

	std::vector<Layer> & layers = scheme->getLayers();
	std::vector<Layer>& newlayers = newScheme->getLayers();

	for (int i = 0; i< layers.size();++i){
		newlayers.push_back(layers[i]);
	}

	newScheme->countSchemeMatrix();
	LOG(INFO, "make scheme \n"<<*newScheme);
//	LOG(INFO, "\n"<<newScheme->getSchemeMatrix());

	IOModule::write(targetMatrixFileName2,0,
				newScheme->getSchemeMatrix(),newScheme->toString());


	for (int i = 0; i<5;i++){
		int rnd = Random::nextRandomInt(10);
		int desc = rnd+10;

		std::vector<int> qbits ;
		qbits.push_back(Random::nextRandomInt(3)+3);
		Layer layer(desc,qbits);

		newlayers.push_back(layer);
	}

	newScheme->countSchemeMatrix();
	LOG(INFO, "make scheme \n"<<*newScheme);
	//LOG(INFO, "\n"<<newScheme->getSchemeMatrix());

	IOModule::write(targetMatrixFileName3,0,
					newScheme->getSchemeMatrix(),newScheme->toString());




	LOG(INFO, "generator is finished")
}
