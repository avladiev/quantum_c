#ifndef GENETIC_ALGORITHMS_HPP_
#define GENETIC_ALGORITHMS_HPP_
#include "model2.hpp"
#include "matrix.hpp"
#include <tr1/memory>
#include <algorithm>
#include "locator.hpp"
class Scheme;
typedef std::tr1::shared_ptr<Scheme> SchemePtr;

enum class GeneticOperator {
	NONE,MUTATION,INVERSION, MATE
};

class Conversion{
public:
	GeneticOperator geneticOperator;
	double oldFitness;
	double newFitness;
};


/**
 * алгоритм скрещевания
 */
class Mate {
public:
	/**
	 * функция скрещевания,
	 * аргументы:
	 * 1 - колекция исходных особей (родители)
	 * 2 - результат  - новая популяция, дети + некоторые родители
	 */
	virtual void mate(std::vector<SchemePtr> & in,
			std::vector<SchemePtr> & out) const= 0;
	virtual std::string toString()const = 0;
	virtual ~Mate() {
	}
	;
};

class MateImpl: public Mate {
private:
	const float factor;
	const float pMate;
	std::pair<SchemePtr, SchemePtr> mate(const SchemePtr & parent1,
			const SchemePtr& parent2) const;
public:
	MateImpl(float pMate, float factor);

	virtual void mate(std::vector<SchemePtr> & in,
			std::vector<SchemePtr> & out) const;
	 std::string toString()const ;
	virtual ~MateImpl();
};

/**
 * алгоритм мутации
 */
class Mutation {
public:
	/**
	 * функция мутации
	 * аргументы:
	 * 1 - вся популяция
	 */

	virtual void mutate(std::vector<SchemePtr> & in) const = 0;
	virtual std::string toString()const = 0;
	virtual ~Mutation() {
	}

};

class MutationImpl: public Mutation {
private:
	const float pMutation;
	const float pInversion;
	void mutate(const SchemePtr & scheme) const;
	void inversion(const SchemePtr & scheme) const;
public:
	MutationImpl(float pMutation, float pInversion);
	virtual void mutate(std::vector<SchemePtr> & in) const;
	virtual std::string toString()const ;
	virtual ~MutationImpl();
};

/*
 * алгоритм отбора
 */
class Selection {
public:
	/**
	 *
	 * аргументы:
	 * 1 - колекция исходных особей упорядоченная по фитнесу.
	 * 2 -результат новая популяция, выживщие особи, их количество  не должено превосходить sizePopulation
	 */
	virtual void selection(std::vector<SchemePtr> & in,
			std::vector<SchemePtr> & out, int sizePopulation) const=0;
	virtual std::string toString()const = 0;
	virtual ~Selection() {

	}
};

class ElitistSelection: public Selection {
public:
	virtual void selection(std::vector<SchemePtr> & in,
			std::vector<SchemePtr> & out, int sizePopulation) const;
	virtual std::string toString()const ;
	virtual ~ElitistSelection();
};

class TourneySelection: public Selection {
public:
	virtual void selection(std::vector<SchemePtr> & in,
			std::vector<SchemePtr> & out, int sizePopulation) const;
	virtual std::string toString()const ;
	virtual ~TourneySelection();
};

class ElitistTourneySelection: public Selection {
private:
	double elitis;
public:
	ElitistTourneySelection(double elitis);
	virtual void selection(std::vector<SchemePtr> & in,
			std::vector<SchemePtr> & out, int sizePopulation) const;
	virtual std::string toString()const ;
	virtual ~ElitistTourneySelection();
};



class Fitness {
public:
	virtual void countFitness(const SchemePtr&) const = 0;
	virtual std::string toString()const = 0;
	virtual ~Fitness() {
	}
};

class FitnessByL2Norm: public Fitness {
public:
	virtual void countFitness(const SchemePtr&) const;
	virtual std::string toString()const ;
	virtual ~FitnessByL2Norm();
};

class FitnessByOperatorL1Norm: public Fitness {
public:
	virtual void countFitness(const SchemePtr&) const;
	virtual ~FitnessByOperatorL1Norm();
};

class FitnessByOperatorL2Norm: public Fitness {
public:
	virtual void countFitness(const SchemePtr&) const;
	virtual ~FitnessByOperatorL2Norm();
};

class FitnessByInfNorm: public Fitness {
public:
	virtual void countFitness(const SchemePtr &) const;
	virtual ~FitnessByInfNorm();
}
;

#endif
