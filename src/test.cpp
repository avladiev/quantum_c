#include <iostream>
#include "io.hpp"
#include<ctime>
#include<cstdlib>
#include "model2.hpp"
#include "Logger.h"
#include "locator.hpp"
#include <unistd.h>
#include <cassert>
#include <algorithm>
#include <vector>

int main(int argc, char* argv[]) {

	Random::init();

	LOG_CONF("log", Logger::file_off|Logger::screen_on, DEBUG, DEBUG, 0);

	std::vector<IMatrix*>* baseMatrix = new std::vector<IMatrix*>();

	baseMatrix->push_back(Gate::getRzGate(M_PI/2));

	for (int i = 0; i<baseMatrix->size();++i){
		std::cout<<" i ="<<i<<"\n"<<*baseMatrix->at(i);
	}
	int numberQubit = 2;
	int sizeMatrix = 1 << (numberQubit);
	int numberLayers = 1;
	MatrixImpl matrix(sizeMatrix);
	Locator::instance().init(baseMatrix, &matrix);
	SchemePtr scheme = Scheme::makeRandomScheme(numberLayers);
	IMatrix & matr= scheme->countSchemeMatrix();

	std::cout<<*scheme;

	std::cout<<matr;

}

