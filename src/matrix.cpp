#include "matrix.hpp"

void IMatrix::multiplication(const IMatrix& m1, const IMatrix& m2,
		IMatrix& out) {
	int thisRows = m1.getNumberRows();
	int thisCols = m1.getNumberColumns();

	int otherRows = m2.getNumberRows();
	int otherCols = m2.getNumberColumns();

	if (thisCols != otherRows) {
		throw IndexBoundException("multiplication, thisCols != otherRows");
	}

	for (int thisRow = 0; thisRow < thisRows; ++thisRow) {
		for (int otherCol = 0; otherCol < otherCols; ++otherCol) {
			Cell sum = 0.0;
			for (int thisCol = 0; thisCol < thisCols; ++thisCol) {
				sum += m1.get(thisRow, thisCol) * m2.get(thisCol, otherCol);
			}
			out.set(thisRow, otherCol, sum);
		}
	}
}

void IMatrix::subtraction(const IMatrix& m1, const IMatrix& m2, IMatrix& out) {
	for (int i = 0; i < m1.getNumberRows(); ++i) {
		for (int j = 0; j < m1.getNumberColumns(); ++j) {
			Cell sub = m1.get(i, j) - m2.get(i, j);
			out.set(i, j, sub);
		}
	}
}
double IMatrix::countSquareL2Norm(const IMatrix& matrix) {
	double result = 0;
	for (int i = 0; i < matrix.getNumberRows(); ++i) {
		for (int j = 0; j < matrix.getNumberColumns(); ++j) {
			Cell v = matrix.get(i, j);
			v = (v * std::conj(v));
			int img = 0;
			assert(v.imag() == 0);
			result += v.real();
		}
	}
	return result;
}

std::ostream & operator<<(std::ostream &out, const IMatrix & m) {
	out << "(" << m.getNumberRows() << "," << m.getNumberColumns() << ")\n";

	for (int i = 0; i < m.getNumberRows(); ++i) {
		for (int j = 0; j < m.getNumberColumns(); ++j) {
			out << m.get(i, j) << " ";
		}
		out << "\n";
	}
	out << "\n";
	return out;
}

double IMatrix::countL1Norm(const IMatrix& matrix) {
	LOG(DEBUG, "fix10");
	LOG(DEBUG, "mat" << matrix);

	double result = 0;
	for (int i = 0; i < matrix.getNumberRows(); ++i) {
		for (int j = 0; j < matrix.getNumberColumns(); ++j) {
			Cell v = matrix.get(i, j);
			v = (v * std::conj(v));
			result += pow(v.real(), 0.5);
		}
	}
	LOG(DEBUG, "fix11");
	return result;
}

double IMatrix::countSquareOperatorL2Norm(const IMatrix& matrix) {
	return countOperatorNorm(matrix, IMatrix::countSquareL2Norm);
}

double IMatrix::countOperatorL1Norm(const IMatrix& matrix) {
	LOG(DEBUG, "fix8")
	return countOperatorNorm(matrix, IMatrix::countL1Norm);
}
double IMatrix::countOperatorNorm(const IMatrix& matrix,
		double (*vectorNorm)(const IMatrix& matrix)) {
	double maxNorma = -1;
	int n = matrix.getNumberColumns();
	int numberElem = matrix.getNumberColumns() * matrix.getNumberRows();
	const float share = 0.70;
	int numberVectors = numberElem * share;
	MatrixImpl xi(matrix.getNumberRows(), 1);
	MatrixImpl yi(matrix.getNumberRows(), 1);
	LOG(DEBUG, "xi\n" << xi)
	for (int i = 0; i < numberVectors; ++i) {
		for (int j = 0; j < xi.getNumberRows(); ++j) {
			Cell cell(Random::nextRandomDouble(), Random::nextRandomDouble());
			LOG(DEBUG, "cell " << cell);
			LOG(DEBUG, "xi\n" << xi)
			LOG(DEBUG, "j= " << j);
			xi.set(j, 0, cell);
		}
		double normaX = vectorNorm(xi);
		if (normaX != 0) {
			IMatrix::multiplication(matrix, xi, yi);
			LOG(DEBUG, "fix12")
			double normaY = vectorNorm(yi);
			double norma = normaY / normaX;
			if (maxNorma < norma) {
				maxNorma = norma;
			}
		}

	}
	return maxNorma;
}

double IMatrix::countSquareInfNorm(const IMatrix & matrix) {
	double max = -1;
	for (int i = 0; i < matrix.getNumberRows(); ++i) {
		for (int j = 0; j < matrix.getNumberColumns(); ++j) {
			Cell v = matrix.get(i, j);
			v = (v * std::conj(v));
			if (max < v.real()) {
				max = v.real();
			}
		}
	}
	return max;
}

void IMatrix::copy(const IMatrix& src, IMatrix& decs) {
	for (int i = 0; i < src.getNumberRows(); ++i) {
		for (int j = 0; j < src.getNumberColumns(); ++j) {
			Cell tmp = src.get(i, j);
			decs.set(i, j, tmp);
		}
	}
}

IMatrix::~IMatrix() {
}

void MatrixImpl::checkBorders(int i, int j) const {
	if (i < numberRows) {
		if (j < numberColumns) {
			return;
		} else {
			std::ostringstream temp;
			temp << "j>= number columns: (i,j) = ( " << i << "," << j << ") ";
			temp << "(rows, columns) = (" << numberRows << "," << numberColumns
					<< ") ";
			throw IndexBoundException(temp.str());
		}
	} else {
		std::ostringstream temp;
		temp << "i>= number rows: (i,j) = ( " << i << "," << j << ") ";
		temp << "(rows, columns) = (" << numberRows << "," << numberColumns
				<< ") ";
		throw IndexBoundException(temp.str());
	}
}

Cell MatrixImpl::get(int i, int j) const {
	checkBorders(i, j);
	if (numberColumns != 1) {
		return data[i * numberRows + j];
	} else {
		return data[i];
	}
}

void MatrixImpl::set(int i, int j, const Cell& val) {
	checkBorders(i, j);
	if (numberColumns != 1) {
		data[i * numberRows + j] = val;
	} else {
		data[i] = val;
	}
}

int MatrixImpl::getNumberRows() const {
	return numberRows;
}

int MatrixImpl::getNumberColumns() const {
	return numberColumns;
}

MatrixImpl::MatrixImpl(int numberRows_, int numberColumns_) :
		numberRows(numberRows_), numberColumns(numberColumns_), data(
				new Cell[numberRows_ * numberColumns_]) {
}

MatrixImpl::MatrixImpl(std::pair<int, int> size) :
		numberRows(size.first), numberColumns(size.second), data(
				new Cell[size.first * size.second]) {
}

MatrixImpl::MatrixImpl(int numberRows_, int numberColumns_, Cell* data_) :
		numberRows(numberRows_), numberColumns(numberColumns_), data(data_) {
}
MatrixImpl::MatrixImpl(int size) :
		numberRows(size), numberColumns(size), data(new Cell[size * size]) {
}

MatrixImpl::MatrixImpl(IMatrix& matrix) :
		numberRows(matrix.getNumberRows()), numberColumns(
				matrix.getNumberColumns()), data(
				new Cell[matrix.getNumberRows() * matrix.getNumberColumns()]) {
	for (int i = 0; i < matrix.getNumberRows(); ++i) {
		for (int j = 0; j < matrix.getNumberColumns(); ++j) {
			set(i, j, matrix.get(i, j));
		}
	}
}
MatrixImpl::~MatrixImpl() {
	delete data;
}
