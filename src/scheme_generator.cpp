#include <iostream>
#include "io.hpp"
#include "model2.hpp"
#include "Logger.h"
#include "locator.hpp"
#include <unistd.h>
#include <cassert>
#include "random.hpp"

SchemePtr makeScheme(int numberLayers, int numberQubit) {
	SchemePtr scheme = Scheme::makeRandomScheme(numberLayers);
	std::vector<int> byQbits;
	byQbits.push_back(Random::nextRandomInt(numberQubit));
	int nextQbit = Random::nextRandomInt(numberQubit);
	while (nextQbit == byQbits[0]) {
		nextQbit = Random::nextRandomInt(numberQubit);
	}
	byQbits.push_back(nextQbit);
	scheme->getLayers()[2] = Layer(2, byQbits);
	return scheme;
}
int main(int argc, char** argv) {

	Random::init();
	LOG_CONF("log", Logger::file_off|Logger::screen_on, DEBUG, DEBUG, 0);

	LOG(INFO, "generator is running");

	std::string baseMatrixFileName;
	std::string targetMatrixFileName;

	if (argc == 1) {
		baseMatrixFileName = "base_matrix_.h5";
		targetMatrixFileName = "targetMatrix_.h5";
	} else {
		baseMatrixFileName = argv[1];
		targetMatrixFileName = argv[2];
	}

	LOG(INFO, "baseMatrixFileName "<<baseMatrixFileName)
	LOG(INFO, "targetMatrixFileName"<<targetMatrixFileName)

	std::vector<IMatrix*>* baseMatrix = //IOModule::readBaseMatrices(
			//baseMatrixFileName);
			new std::vector<IMatrix*>();

	baseMatrix->push_back(Gate::getHGate());
	baseMatrix->push_back(Gate::getXGate());
	baseMatrix->push_back(Gate::getCNotGate());
	baseMatrix->push_back(Gate::getZGate());

	double eps = 0.0001;
	for (double alpha = 0; alpha < 2 * M_PI + eps; alpha += M_PI / 6) {
		baseMatrix->push_back(Gate::getRxGate(alpha));
		baseMatrix->push_back(Gate::getRyGate(alpha));
		baseMatrix->push_back(Gate::getRzGate(alpha));
	}

	int numberQubit = 2;
	int sizeMatrix = 1 << (numberQubit);
	int numberLayers = 5;
	MatrixImpl matrix(sizeMatrix);
	Locator::instance().init(baseMatrix, &matrix);

	SchemePtr scheme = makeScheme(numberLayers, numberQubit);

	while (true) {
		std::vector<Layer> & layers = scheme->getLayers();
		bool check1 = true;
		for (int numQubit = 0; numQubit < numberQubit; ++numQubit) {
			bool check2 = false;
			for (int i = 0; i < layers.size(); ++i) {
				Layer & layer = layers[i];
				if (layer.byQubit(numQubit)) {
					check2 = true;
					break;
				}
			}
			if (!check2) {
				check1 = false;
				break;
			}
		}

		if (check1) {
			break;
		}
		scheme = makeScheme(numberLayers, numberQubit);

	}

	LOG(INFO, "make scheme <<--\n"<<*scheme<<"-->>");
	scheme->countSchemeMatrix();
	LOG(INFO, "\n"<<scheme->getSchemeMatrix()<<"");
	IOModule::write(targetMatrixFileName, 0, scheme->getSchemeMatrix(),
			scheme->toString());
	//IOModule::writeBaseMatrices(baseMatrixFileName, baseMatrix);
	LOG(INFO, "generator is finished")
}
