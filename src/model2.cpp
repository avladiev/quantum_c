#include "model2.hpp"
#include "locator.hpp"
#include <algorithm>
#include <utility>
#include "random.hpp"

IMatrix* Gate::getRzGate(double alpha) {
	IMatrix *matrix = new MatrixImpl(2);
	double real = cos(alpha / 2);
	double imag = sin(alpha / 2);
	matrix->set(0, 0, Cell(real, -imag));
	matrix->set(1, 1, Cell(real, imag));
	return matrix;
}

IMatrix* Gate::getRyGate(double alpha) {
	IMatrix *matrix = new MatrixImpl(2);
	double cosVal = cos(alpha / 2);
	double sinVal = sin(alpha / 2);
	matrix->set(0, 0, Cell(cosVal, 0));
	matrix->set(0, 1, Cell(-sinVal, 0));
	matrix->set(1, 0, Cell(sinVal, 0));
	matrix->set(1, 1, Cell(cosVal, 0));
	return matrix;
}

IMatrix* Gate::getRxGate(double alpha) {
	IMatrix *matrix = new MatrixImpl(2);
	double cosVal = cos(alpha / 2);
	double sinVal = sin(alpha / 2);
	matrix->set(0, 0, Cell(cosVal, 0));
	matrix->set(0, 1, Cell(0, -sinVal));
	matrix->set(1, 0, Cell(0, -sinVal));
	matrix->set(1, 1, Cell(cosVal, 0));
	return matrix;
}

IMatrix* Gate::getHGate() {
	IMatrix *matrix = new MatrixImpl(2);
	double number = 1 / pow(2, 0.5);
	Cell cell(number, 0);
	matrix->set(0, 0, cell);
	matrix->set(0, 1, cell);
	matrix->set(1, 0, cell);
	cell = Cell(-number, 0);
	matrix->set(1, 1, cell);
	return matrix;

}
IMatrix* Gate::getXGate() {
	IMatrix *matrix = new MatrixImpl(2);
	matrix->set(0, 1, Cell(1, 0));
	matrix->set(1, 0, Cell(1, 0));
	return matrix;
}

IMatrix * Gate::getCNotGate() {
	IMatrix *matrix = new MatrixImpl(4);
	Cell cell(1, 0);
	matrix->set(0, 0, cell);
	matrix->set(1, 1, cell);
	matrix->set(2, 3, cell);
	matrix->set(3, 2, cell);
	return matrix;
}

IMatrix * Gate::getZGate() {
	IMatrix * matrix = new MatrixImpl(2);
	Cell cell1(1, 0);
	Cell cell2(-1, 0);

	matrix->set(0, 0, cell1);
	matrix->set(1, 1, cell2);
	return matrix;
}

/*Gate::Gate(){
 }
 IMatrix& Gate::getMatrix(){
 return *matrix;
 }
 int Gate::getNumberQubit(){
 return numberQubit;
 }

 Gate::~Gate(){
 delete matrix;
 }

 RzGate::RzGate(double alpha){
 matrix = new MatrixImpl(2);
 numberQubit = Util::countNumberQubit(matrix->getNumberRows());
 Cell cell;
 cell.real(cos(alpha/2));
 cell.imag(-sin(alpha/2));
 matrix->set(0,0,cell);
 cell.imag((sin(alpha/2)));
 matrix->set(1,1,cell);
 }*/

Layer::Layer() {

}
// impl Layer class methods
Layer::Layer(const int baseMatrixDesc_, const std::vector<int>& qubits) :
		baseMatrixDesc(baseMatrixDesc_) {
	int numberQubitInScheme = Locator::instance().getNumberQubitInScheme();
	byQubits = qubits;
	std::sort(byQubits.begin(), byQubits.end());
}

Layer Layer::makeRandomLayer() {
	int numberBaseMatrix = Locator::instance().getNumberBaseMatrix();
	int desc = Random::nextRandomInt(numberBaseMatrix);
	const IMatrix & levelMatix = Locator::instance().getBaseMatrix(desc);
	int numberMatrixQ = Util::countNumberQubit(levelMatix.getNumberRows());
	std::vector<int> qubits;
	int numberQubitInScheme = Locator::instance().getNumberQubitInScheme();
	while (qubits.size() < numberMatrixQ) {
		int num = Random::nextRandomInt(numberQubitInScheme);
		if (std::find(qubits.begin(), qubits.end(), num) == qubits.end()) {
			/*  does not contain num */
			qubits.push_back(num);
		}
	}
	Layer level(desc, qubits);
	return level;
}

Cell Layer::countLayerMatrixCell(const int i, const int j) {
	static std::bitset<32> bsI;
	static std::bitset<32> bsJ;
	bsI = i;
	bsJ = j;
	int numberQubitInScheme = Locator::instance().getNumberQubitInScheme();
	const IMatrix & baseMatrix = Locator::instance().getBaseMatrix(
			baseMatrixDesc);
	Cell res = 1;
	int indUi = 0;
	int indUj = 0;
	int foundationX = 1;
	for (int k = 0; k < numberQubitInScheme; ++k) {
		if (!byQubit(k)) { //!contains
			res *= (bsI[k] == bsJ[k] ? 1 : 0);
		} else {
			indUi += foundationX * bsI[k];
			indUj += foundationX * bsJ[k];
			foundationX <<= 1;
		}
	}
	res *= baseMatrix.get(indUi, indUj);
	return res;
}

IMatrix * Layer::countLayerMatrix() {
	std::pair<int, int> size = Locator::instance().getSizeTargetMatrix();
	IMatrix * layerMatrix = new MatrixImpl(size.first, size.second);

	for (int i = 0; i < layerMatrix->getNumberRows(); ++i) {
		for (int j = 0; j < layerMatrix->getNumberColumns(); ++j) {
			Cell val = countLayerMatrixCell(i, j);
			layerMatrix->set(i, j, val);
		}
	}
	return layerMatrix;
}
bool Layer::byQubit(int qubit) {
	return std::binary_search(byQubits.begin(), byQubits.end(), qubit);
}
int Layer::getBaseMatrixDesc() {
	return baseMatrixDesc;
}

Layer::~Layer() {

}
//impl Scheme methods

Scheme::Scheme() {
	schemeMatrix = new MatrixImpl(Locator::instance().getSizeTargetMatrix());
	fitness = 1. / 0.0;
}

Scheme::Scheme(const Layer& firstLevel) {
	schemeMatrix = new MatrixImpl(Locator::instance().getSizeTargetMatrix());
	layers.push_back(firstLevel);
	fitness = 1. / 0.0;
}
Scheme::Scheme(Scheme& scheme) {
	schemeMatrix = new MatrixImpl(scheme.getSchemeMatrix());
	fitness = scheme.getFitness();
	std::vector<Layer> & layers = scheme.getLayers();
	for (int i = 0; i < layers.size(); ++i) {
		this->layers.push_back(layers[i]);
	}
}

SchemePtr Scheme::makeRandomScheme(int size) {
	SchemePtr scheme(new Scheme());
	std::vector<Layer>& layers = scheme->getLayers();
	for (int i = 0; i < size; ++i) {
		layers.push_back(Layer::makeRandomLayer());
	}
	return scheme;
}

double Scheme::getFitness() const {
	return fitness;
}
std::vector<Layer> & Scheme::getLayers() {
	return layers;
}

IMatrix& Scheme::countSchemeMatrix() {
	std::pair<int, int> size = Locator::instance().getSizeTargetMatrix();
//todo make method in  Matrix identity matrix
	for (int i = 0; i < schemeMatrix->getNumberRows(); ++i) {
		for (int j = 0; j < schemeMatrix->getNumberColumns(); ++j) {
			double val = i == j ? 1 : 0;
			schemeMatrix->set(i, j, val);
		}
	}

	IMatrix * tmpMatrix = new MatrixImpl(schemeMatrix->getNumberRows(),
			schemeMatrix->getNumberColumns());

	for (int i = 0; i < layers.size(); ++i) {
		IMatrix * levelMatrix = layers[i].countLayerMatrix();
		IMatrix::multiplication(*schemeMatrix, *levelMatrix, *tmpMatrix);
		delete levelMatrix;
		std::swap(schemeMatrix, tmpMatrix);
	}

	delete tmpMatrix;
	return *schemeMatrix;
}

IMatrix & Scheme::getSchemeMatrix() {
	return *schemeMatrix;
}

void Scheme::setFitness(double fitness) {
	this->fitness = fitness;
}

void Scheme::setOldFitness(double fitness) {
	oldFitness = fitness;
}

std::string Scheme::toString() {
	std::ostringstream temp;
	temp << *this;
	return temp.str();
}

bool Scheme::isMate() {
	return mate;
}

bool Scheme::isMutate() {
	return mutate;
}

bool Scheme::isInversion() {
	return inversion;
}

bool Scheme::isAppliedGAOperator(){
	return inversion || mate || mutate;
}
double Scheme::getOldFitness() {
	return oldFitness;
}

void Scheme::setMate() {
	mate = true;
}
void Scheme::setMutate() {
	mutate = true;
}
void Scheme::setInversion() {
	inversion = true;
}

Scheme::~Scheme() {
	delete schemeMatrix;
}

std::ostream & operator<<(std::ostream &out, Scheme & scheme) {
	std::vector<Layer> &layers = scheme.getLayers();
	out << "fitness =" << scheme.getFitness() << ", numberLayers = "
			<< layers.size() << "\n";
	for (int qubit = Locator::instance().getNumberQubitInScheme() - 1;
			qubit >= 0; --qubit) {

		for (int j = 0; j < layers.size(); ++j) {
			out << "|--";
			Layer & layer = layers[j];
			if (layer.byQubit(qubit)) {
				out << "" << layer.getBaseMatrixDesc() << "-";
			} else {
				out << "--";
			}
		}
		out << "|\n";
	}
	return out;
}
// impl AlgorithmContainerl class methods
AlgorithmContainer::AlgorithmContainer(const Mate& mate_,
		const Mutation& mutation_, const Fitness & fitness_,
		const Selection& selection_) :
		mate(mate_), mutation(mutation_), fitness(fitness_), selection(
				selection_) {

}

//impl Population methods
Population::Population(const AlgorithmContainer& algorithmContainer_,
		int sizePopulation_) :
		sizePopulation(sizePopulation_), algoContainer(algorithmContainer_) {
	schemes = new std::vector<SchemePtr>();
	buffer = new std::vector<SchemePtr>();
	int numberBaseMatrix = Locator::instance().getNumberBaseMatrix();
	//создается популяция
	LOG(INFO, "create population\n");
	for (int i = 0; i < sizePopulation; ++i) {
		Layer layer = Layer::makeRandomLayer();
		SchemePtr schemePtr(new Scheme(layer));
		schemes->push_back(schemePtr);
		LOG(INFO, "random "<<i<<"scheme\n"<<*schemePtr)
	}
}
SchemePtr Population::getFirstScheme() {
	if (schemes->size() < 1) {
		LOG(ERROR, "schemes->size()<1");
	}
	return (schemes->at(0));
}
int Population::getCurrentSize() {
	return schemes->size();
}

std::vector<SchemePtr> & Population::getSchemes() {
	return *schemes;
}
void Population::mate() {
	buffer->clear();
	algoContainer.mate.mate(*schemes, *buffer);
	std::swap(schemes, buffer);

}
void Population::mutate() {
	algoContainer.mutation.mutate(*schemes);
}

void Population::countFitness() {
	for (int i = 0; i < schemes->size(); ++i) {
		const SchemePtr& schemePtr = schemes->at(i);
		algoContainer.fitness.countFitness(schemePtr);
	}
}
bool functor(const SchemePtr& i, const SchemePtr & j) {
	return i->getFitness() < j->getFitness();
}
void Population::sort() {
	std::sort(schemes->begin(), schemes->end(), functor);
}

void Population::selection() {
	buffer->clear();
	algoContainer.selection.selection(*schemes, *buffer, sizePopulation);
	std::swap(schemes, buffer);
}

Population::~Population() {
	delete schemes;
	delete buffer;
}

std::ostream & operator<<(std::ostream &out, Population& popul) {
	std::vector<SchemePtr> & sch = popul.getSchemes();
	for (int i = 0; i < sch.size(); ++i) {
		const SchemePtr& tmp = sch.at(i);
		out << (*tmp) << "\n";
	}
	return out;
}
//impl Model methods

Model::Model(const AlgorithmContainer& algorithmContainer,
		const int sizePopulation, const int numberStepEvolution_,
		const double maxFitness_) :
		numberStepEvolution(numberStepEvolution_), desiredFitness(maxFitness_) {
	population = new Population(algorithmContainer, sizePopulation);
}

void Model::logFitness(std::ofstream & logFitnessStream,
		std::ofstream & statisticsStream,
		const std::vector<SchemePtr> & schemes) {
	std::ostringstream buf1;
	std::string delimiter = "";
	std::vector<int> statistics;
	statistics.resize(12, 0);
	statistics[0] = schemes.size();
	double eps = pow(10, -8);
	for (int i = 0; i < schemes.size(); ++i) {
		buf1 << delimiter << schemes[i]->getFitness();
		delimiter = ",";
		double changeFitness = schemes[i]->getOldFitness()
				- schemes[i]->getFitness();

		if (abs(changeFitness) < eps) {
			//not change
			if (schemes[i]->isMutate()) {
				statistics[3]++;
			}

			if (schemes[i]->isInversion()) {
				statistics[6]++;
			}

			if (schemes[i]->isMate()) {
				statistics[9]++;
			}

		} else if (changeFitness > 0) {
			//become best
			statistics[1]++;

			if (schemes[i]->isMutate()) {
				statistics[3]++;
				statistics[4]++;
			}

			if (schemes[i]->isInversion()) {
				statistics[6]++;
				statistics[7]++;

			}

			if (schemes[i]->isMate()) {
				statistics[9]++;
				statistics[10]++;
			}

		} else if (changeFitness < 0) {
			//become worse
			statistics[2]++;

			if (schemes[i]->isMutate()) {
				statistics[3]++;
				statistics[5]++;
			}

			if (schemes[i]->isInversion()) {
				statistics[6]++;
				statistics[8]++;
			}

			if (schemes[i]->isMate()) {
				statistics[9]++;
				statistics[11]++;
			}
		} else {
			LOG(ERROR,
					"does not have a branch for changeFitness = "<<changeFitness);
		}
	}

	logFitnessStream << buf1.str() << "\n";
	/*delimiter = "";
	for (int i = 0; i<schemes.size();++i){
		buf1 << delimiter << schemes[i]->getOldFitness();
				delimiter = ",";
	}
	logFitnessStream << buf1.str() << "\n";
*/
	delimiter = "";
	for (int i = 0; i < statistics.size(); ++i) {
		statisticsStream << delimiter << statistics[i];
		delimiter = ",";
	}
	statisticsStream << "\n";
}

SchemePtr Model::run(const std::string &logFitnessFileName,
		const std::string logStatisticsFileName) {
	std::ofstream logFitnessStream(logFitnessFileName.c_str(),
			std::ofstream::out);

	std::ofstream logStaticticStream(logStatisticsFileName, std::ofstream::out);

	LOG(INFO, "run evolution");
	SchemePtr bestScheme(new Scheme());
	std::cerr << "best<<" << *bestScheme << "\n";
	for (int i = 0; i < numberStepEvolution; ++i) {
		LOG(INFO, "step evolution:" << i);

		population->countFitness();
		population->sort();
		SchemePtr firstScheme = population->getFirstScheme();
		LOG(INFO, "best fitness:" << firstScheme->getFitness());
		LOG(INFO, "best SCHEME:\n" << *bestScheme);
		LOG(INFO, "first SCHEME:\n" << *firstScheme);
		LOG(DEBUG, "size before selection " << population->getCurrentSize());
		population->selection();
		LOG(DEBUG, "size after selection " << population->getCurrentSize());
		logFitness(logFitnessStream, logStaticticStream,
				population->getSchemes());
		if (firstScheme->getFitness() < bestScheme->getFitness()) {
			bestScheme = SchemePtr(new Scheme(*firstScheme));
			if (firstScheme->getFitness() < desiredFitness) {
				break;
			}
		} else {
			population->getSchemes().push_back(
					SchemePtr(new Scheme(*bestScheme)));
		}

		if (i < numberStepEvolution) {
			population->mate();
			population->mutate();
		}

	}
	logFitnessStream.close();
	logStaticticStream.close();
	return bestScheme;
}

Model::~Model() {
	delete population;
}

