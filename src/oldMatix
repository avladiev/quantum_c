#ifndef MATRIX_HPP_
#define MATRIX_HPP_
#include "exeptions.hpp"
#include <sstream>
#include <string>
#include <bitset>
#include "iostream"
#include <complex>
#include "Logger.h"
#include "random.hpp"

template<typename T>
class Matrix;
template<typename T>
class SimpleMatrix;

typedef std::complex<double> Cell;
typedef Matrix<Cell> IMatrix;
typedef SimpleMatrix<Cell> MatrixImpl;

template<typename T>
std::ostream & operator<<(std::ostream &out, const Matrix<T> & m) {
	out << "(" << m.getNumberRows() << "," << m.getNumberColumns() << ")\n";

	for (int i = 0; i < m.getNumberRows(); ++i) {
		for (int j = 0; j < m.getNumberColumns(); ++j) {
			out << m.get(i, j) << " ";
		}
		out << "\n";
	}
	out << "\n";
	return out;
}

template<typename T>
class Matrix {
public:
	virtual T get(int i, int j) const = 0;
	virtual void set(int i, int j, const T & val) = 0;
	virtual int getNumberRows() const = 0;
	virtual int getNumberColumns() const= 0;

	static void multiplication(const Matrix<T> & m1, const Matrix<T> &m2,
			Matrix<T> & out) {

		int thisRows = m1.getNumberRows();
		int thisCols = m1.getNumberColumns();

		int otherRows = m2.getNumberRows();
		int otherCols = m2.getNumberColumns();

		if (thisCols != otherRows) {
			throw IndexBoundException("multiplication, thisCols != otherRows");
		}

		for (int thisRow = 0; thisRow < thisRows; ++thisRow) {
			for (int otherCol = 0; otherCol < otherCols; ++otherCol) {
				T sum = 0.0;
				for (int thisCol = 0; thisCol < thisCols; ++thisCol) {
					sum += m1.get(thisRow, thisCol) * m2.get(thisCol, otherCol);
				}
				out.set(thisRow, otherCol, sum);

			}
		}

	}

	static void subtraction(const Matrix<T> & m1, const Matrix<T> &m2,
			Matrix<T> & out) {
		for (int i = 0; i < m1.getNumberRows(); ++i) {
			for (int j = 0; j < m1.getNumberColumns(); ++j) {
				T sub = m1.get(i, j) - m2.get(i, j);
				out.set(i, j, sub);
			}
		}
	}
	/**
	 * считает квадрат эвклидовой нормы
	 */
	static double countSquareL2Norm(const Matrix<T> & matrix) {
		double result = 0;
		for (int i = 0; i < matrix.getNumberRows(); ++i) {
			for (int j = 0; j < matrix.getNumberColumns(); ++j) {
				T v = matrix.get(i, j);
				v = (v * std::conj(v));
				int img = 0;
				assert(v.imag() == 0);
				result += v.real();
			}
		}
		return result;
	}

	static double countL1Norm(const Matrix & matrix) {
		LOG(DEBUG, "fix10");
		LOG(DEBUG, "mat"<<matrix);

		double result = 0;

		for (int i = 0; i < matrix.getNumberRows(); ++i) {
			for (int j = 0; j < matrix.getNumberColumns(); ++j) {
				T v = matrix.get(i, j);
				v = (v * std::conj(v));
				result += pow(v.real(), 0.5);
			}
		}
		LOG(DEBUG, "fix11");
		return result;
	}

	/**
	 * считает квадрат нормы maxij[aij]
	 */
	static double countSquareInfNorm(const Matrix<T> & matrix) {
		double max = -1;
		for (int i = 0; i < matrix.getNumberRows(); ++i) {
			for (int j = 0; j < matrix.getNumberColumns(); ++j) {
				T v = matrix.get(i, j);
				v = (v * std::conj(v));
				if (max < v.real()) {
					max = v.real();
				}
			}
		}
		return max;
	}

	/**
	 * считает квадрат операторной нормы по L2
	 */
	static double countSquareOperatorL2Norm(const Matrix<T> & matrix) {
		return countOperatorNorm(matrix, Matrix<T>::countSquareL2Norm);
	}
	/**
	 * считает операторную норму по L1
	 */
	static double countOperatorL1Norm(const Matrix<T> & matrix) {
		LOG(DEBUG, "fix8")
		return countOperatorNorm(matrix, Matrix<T>::countL1Norm);
	}

	/**
	 * считает операторную норму
	 */
	static double countOperatorNorm(const Matrix<T> & matrix,
			double (*vectorNorm)(const Matrix<T> & matrix)) {
		double maxNorma = -1;
		int n = matrix.getNumberColumns();
		int numberElem = matrix.getNumberColumns() * matrix.getNumberRows();
		const float share = 0.25;
		int numberVectors = numberElem * share;
		SimpleMatrix<T> xi(matrix.getNumberRows(), 1);
		SimpleMatrix<T> yi(matrix.getNumberRows(), 1);
		LOG(DEBUG,"xi\n"<<xi)
		for (int i = 0; i < numberVectors; ++i) {
			for (int j = 0; j < xi.getNumberRows(); ++j) {
				T cell(Random::nextRandomDouble(),
						Random::nextRandomDouble());

				LOG(DEBUG,"cell "<<cell);
				LOG(DEBUG,"xi\n"<<xi)
				LOG(DEBUG,"j= "<<j);
				xi.set(j, 0,cell);
			}

			/*Matrix<T>::multiplication(matrix, xi, yi);
			LOG(DEBUG, "fix12")
			double norma = vectorNorm(yi) / vectorNorm(xi);
			if (maxNorma < norma) {
				maxNorma = norma;
			}*/

		}
		return maxNorma;
	}

	static void copy(const Matrix<T>& src, Matrix<T>& decs) {
		for (int i = 0; i < src.getNumberRows(); ++i) {
			for (int j = 0; j < src.getNumberColumns(); ++j) {
				T tmp = src.get(i, j);
				decs.set(i, j, tmp);
			}
		}
	}

	virtual ~Matrix() {
	}
};

template<typename T>
class SimpleMatrix: public Matrix<T> {
private:
	T* data;
	const int numberRows;
	const int numberColumns;
	SimpleMatrix<T> &operator =(const SimpleMatrix<T> &);
	void checkBorders(int i, int j) const {
		if (i < numberRows) {
			if (j < numberColumns) {
				return;
			} else {
				std::ostringstream temp;
				temp << "j>= number columns: (i,j) = ( " << i << "," << j
						<< ") ";
				temp << "(rows, columns) = (" << numberRows << ","
						<< numberColumns << ") ";
				throw IndexBoundException(temp.str());
			}
		} else {
			std::ostringstream temp;
			temp << "i>= number rows: (i,j) = ( " << i << "," << j << ") ";
			temp << "(rows, columns) = (" << numberRows << "," << numberColumns
					<< ") ";
			throw IndexBoundException(temp.str());
		}
	}

public:
	SimpleMatrix(int numberRows_, int numberColumns_) :
			numberRows(numberRows_), numberColumns(numberColumns_), data(
					new T[numberRows_ * numberColumns_]) {
	}

	SimpleMatrix(std::pair<int, int> size) :
			numberRows(size.first), numberColumns(size.second), data(
					new T[size.first * size.second]) {

	}

	SimpleMatrix(int numberRows_, int numberColumns_, T* data_) :
			numberRows(numberRows_), numberColumns(numberColumns_), data(data_) {
	}

	SimpleMatrix(int size) :
			numberRows(size), numberColumns(size), data(new T[size * size]) {

	}
	SimpleMatrix(Matrix<T> &matrix) :
			numberRows(matrix.getNumberRows()), numberColumns(
					matrix.getNumberColumns()), data(
					new T[matrix.getNumberRows() * matrix.getNumberColumns()]) {
		for (int i = 0; i < matrix.getNumberRows(); ++i) {
			for (int j = 0; j < matrix.getNumberColumns(); ++j) {
				set(i, j, matrix.get(i, j));
			}
		}
	}
	virtual int getNumberRows() const {
		return numberRows;
	}

	virtual int getNumberColumns() const {
		return numberColumns;
	}

	virtual T get(int i, int j) const {
		checkBorders(i, j);
		return data[i * numberRows + j];
	}

	virtual void set(int i, int j, const T & val) {
		checkBorders(i, j);
		data[i * numberRows + j] = val;
	}

	virtual ~SimpleMatrix() {
		delete[] data;
	}

};

#endif
