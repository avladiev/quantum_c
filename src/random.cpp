#include "random.hpp"
#include<ctime>
#include<cstdlib>


void Random::init(){
	srand(unsigned(time(NULL)));
	//srand(unsigned(gettimeofday(NULL)));
}
int Random::nextRandomInt(const int maxNumber) {

	return rand() % maxNumber;
}
 double Random::nextRandomDouble() {
	double rand = 1.0 * Random::nextRandomInt(RAND_MAX);
	return rand / RAND_MAX;
}


