#include "util.hpp"
#include "math.h"
std::string Util::intToString(int a) {
	std::ostringstream temp;
	temp << a;
	return temp.str();
}
std::string Util::doubleToString(double a) {
	std::ostringstream temp;
	temp << a;
	return temp.str();
}

int Util::countNumberQubit(const int row) {
	return log(row) / log(2);
}

std::string Util::matrixToStr(std::vector<IMatrix*>* matrices) {
	std::ostringstream oss;
	for (int i = 0; i < matrices->size(); ++i) {
		oss << "matrix " << i << "\n" << (*matrices->at(i)) << "\n";
	}

	return oss.str();
}

