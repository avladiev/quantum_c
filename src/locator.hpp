#ifndef LOCATOR_HPP_
#define LOCATOR_HPP_

#include "matrix.hpp"
#include <vector>
#include "util.hpp"
#include "io.hpp"

template<class T>
class Singleton {
private:
	static T* myInstance;

public:
	static T& instance() {
		if (myInstance == 0) {
			myInstance = new T;
		}
		return *myInstance;
	}

protected:
	Singleton() {
	}
private:
	Singleton& operator=(const Singleton&) {
		return *this;
	}
	Singleton(const Singleton&) {
	}
};

class Locator: public Singleton<Locator> {
private:
	std::vector<IMatrix*>* baseMatrix;
	IMatrix * targetMatrix;
	int numberQubitInScheme;
public:
	Locator();
	virtual ~Locator();
	void init(std::vector<IMatrix*>* baseMatrix, IMatrix* targetMatrix);
	int getNumberBaseMatrix();
	const IMatrix& getBaseMatrix(int desc);
	const IMatrix& getTargetMatrix();
	int getNumberQubitInScheme();

	std::pair<int,int> getSizeTargetMatrix();

	friend class Singleton<Locator> ;
};

#endif
