
#ifndef UTIL_HPP_
#define UTIL_HPP_
#include <sstream>
#include <string>
#include <cstdlib>
#include "matrix.hpp"
#include <vector>


class Util {

public:

	static std::string intToString(int a) ;
	static std::string doubleToString(double a) ;
	/*
	 * считает количество кубит на которые действует матрица: log_2(row);
	 */
	static int countNumberQubit(const int row);

	static std::string matrixToStr(std::vector<IMatrix*>* matrices);


};

#endif
