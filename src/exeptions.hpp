#ifndef EXEPTIONS_HPP_
#define EXEPTIONS_HPP_
#include <exception>
#include <string>

class IndexBoundException: public std::exception {
public:
	/** Constructor (C strings).
	 *  @param message C-style string error message.
	 *                 The string contents are copied upon construction.
	 *                 Hence, responsibility for deleting the \c char* lies
	 *                 with the caller.
	 */
	explicit IndexBoundException(const char* message) :
			msg_(message) {
	}

	/** Constructor (C++ STL strings).
	 *  @param message The error message.
	 */
	explicit IndexBoundException(const std::string& message) :
			msg_(message) {
	}

	/** Destructor.
	 * Virtual to allow for subclassing.
	 */
	virtual ~IndexBoundException() throw () {
	}

	/** Returns a pointer to the (constant) error description.
	 *  @return A pointer to a \c const \c char*. The underlying memory
	 *          is in posession of the \c Exception object. Callers \a must
	 *          not attempt to free the memory.
	 */
	virtual const char* what() const throw () {
		return msg_.c_str();
	}

protected:
	/** Error message.
	 */
	std::string msg_;
};

#endif
