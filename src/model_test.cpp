#include <iostream>
#include "model.hpp"

void testLevel1(){
	double* data = new double[4];
	data[0] = 0;
	data[1]= 1;
	data[2] = 1;
	data[3] = 0; // not
	Matrix<double> * base = new SimpleMatrix<double>(2,2,data);
	double *dataRes = new double[16];
	for (int i = 0; i<16;++i){
		if (i==2 || i==7 || i==8 ||i==13){
			dataRes[i] = 1;
		}else{
			dataRes[i] = 0;
		}
	}
	Matrix<double> * levelResultMatrix = new SimpleMatrix<double>(4,4,dataRes);
	std::cout<<*levelResultMatrix;
	int qubits[] = {0};
	Level level(*base,2,qubits,1);
	std::cout<<"finish init\n";
	 Matrix<double> const & levelMatrix  =level.getLevelMatrix();
	std::cout<<"levelMatrix before count\n"<<levelMatrix;
	Matrix<double> const & levelMatrix2= level.countLevelMatrix();


	std::cout<<"levelMatrix after count\n"<<levelMatrix2;

	std::cout<<"finish test\n";

	delete base;
	delete levelResultMatrix;
}


void testLevel2(){
	double* data = new double[4];
	data[0] = 0;
	data[1]= 1;
	data[2] = 1;
	data[3] = 0; // not
	Matrix<double> * base = new SimpleMatrix<double>(2,2,data);
	int qubits[] = {2};
	Level level(*base,3,qubits,1);
	std::cout<<"finish init\n";
	 Matrix<double> const & levelMatrix  =level.getLevelMatrix();
	std::cout<<"levelMatrix before count\n"<<levelMatrix;
	Matrix<double> const & levelMatrix2= level.countLevelMatrix();


	std::cout<<"levelMatrix after count\n"<<levelMatrix2;

	std::cout<<"finish test\n";

	delete base;
}

void testLevel3(){
	Matrix<double> * base = new SimpleMatrix<double>(4);//Cnot
	base->set(0,0,1);
	base->set(1,1,1);
	base->set(2,3,1);
	base->set(3,2,1);
	std::cout<<*base;
	int qubits[] = {0,2};
	Level level(*base,3,qubits,2);
	std::cout<<"finish init\n";
	 Matrix<double> const & levelMatrix  =level.getLevelMatrix();
	std::cout<<"levelMatrix before count\n"<<levelMatrix;
	Matrix<double> const & levelMatrix2= level.countLevelMatrix();


	std::cout<<"levelMatrix after count\n"<<levelMatrix2;

	std::cout<<"finish test\n";

	delete base;
}
int main(){

	testLevel3();
}
