#ifndef MODEL2_HPP_
#define MODEL2_HPP_

#include "matrix.hpp"
#include <set>
#include "Logger.h"
#include <vector>
#include "genetic_algorithms.hpp"
#include <tr1/memory>
class Mutation;
class Mate;
class Selection;
class Fitness;

class Gate {
public:
	static IMatrix* getRzGate(double alpha);
	static IMatrix*getRyGate(double alpha);
	static IMatrix* getRxGate(double alpha);

	static IMatrix* getHGate();
	static IMatrix* getXGate();
	static IMatrix * getCNotGate();
	static IMatrix * getZGate();

};
/*class Gate {
 private:
 Gate & operator=(const Gate&);
 Gate(const Gate &);
 protected:
 IMatrix *matrix;
 int numberQubit;
 Gate();
 public:
 virtual IMatrix & getMatrix();
 virtual int getNumberQubit();
 virtual ~Gate();
 };*/

/*
 class RzGate: public Gate {
 private:
 RzGate(const RzGate&);
 RzGate & operator=(const RzGate&);
 public:
 RzGate(double alpha);
 };
 */

class Layer {
private:
	/**
	 * дискриптор на базовую матрицу. саму матрицу можно получить Locator::instance().getBaseMatrix(desc);
	 */

	int baseMatrixDesc;
	/**
	 * // те кубиты по которым действует матрица.  ex нумерауии: число ABCD , A -третий, D-нулевой
	 * этот масиив отсортирован.
	 */
	std::vector<int> byQubits;

	/**
	 *  в этом методе  используется bitset если количествл кубит будет >32 будут проблемы
	 */
	Cell countLayerMatrixCell(const int i, const int j);

public:
	Layer();
	Layer(const int baseMatrixDesc, const std::vector<int>& qubits);
	static Layer makeRandomLayer();
	IMatrix * countLayerMatrix();
	int getBaseMatrixDesc();
	bool byQubit(int qubit);

	~Layer();
};

class Scheme;
typedef std::tr1::shared_ptr<Scheme> SchemePtr;
/**
 * класс, который моделирует особь.
 * особь представляет собой коллекцию Layer
 */
class Scheme {
private:
	double fitness;
	std::vector<Layer> layers;
	IMatrix *schemeMatrix;
	Scheme & operator=(const Scheme&);
	double oldFitness;

	bool mate = false;
	bool mutate = false;
	bool inversion = false;

public:
	Scheme();
	Scheme(const Layer & firstLayer);
	Scheme(Scheme&);

	static SchemePtr makeRandomScheme(int size);
	double getFitness() const;
	std::vector<Layer> & getLayers();
	IMatrix & countSchemeMatrix();
	IMatrix & getSchemeMatrix();
	void setFitness(double);
	void setOldFitness(double);
	std::string toString();

	bool isMate();
	bool isMutate();
	bool isInversion();

	bool isAppliedGAOperator();

	void setMate();
	void setMutate();
	void setInversion();

	double getOldFitness();

	~Scheme();
};

std::ostream & operator<<(std::ostream &out, Scheme & id);

/**
 * класс для удобства
 */
class AlgorithmContainer {
public:
	const Mate& mate;
	const Mutation& mutation;
	const Fitness & fitness;
	const Selection & selection;
	AlgorithmContainer(const Mate& mate_, const Mutation& mutation_,
			const Fitness & fitness_, const Selection & selection);

};

/*
 * моделирует популяцию.
 */
class Population {
private:
	const int sizePopulation;
	std::vector<SchemePtr>* schemes;
	std::vector<SchemePtr>* buffer;
	const AlgorithmContainer & algoContainer;
	Population(const Population&);
	Population& operator=(const Population&);

public:
	Population(const AlgorithmContainer& algorithmContainer,
			int sizePopulation);
	void mate();
	void mutate();
	void selection();
	void countFitness();
	void sort();
	SchemePtr getFirstScheme();
	int getCurrentSize();
	std::vector<SchemePtr> & getSchemes();
	~Population();
};
std::ostream & operator<<(std::ostream &out, Population& popul);

class Model {
private:
	const double desiredFitness;
	const int numberStepEvolution;
	Population* population;
	void logFitness(std::ofstream & logFitnessStream,std::ofstream & statisticsStream,
			const std::vector<SchemePtr> & schemes);
	bool makeSelection();
public:
	Model(const AlgorithmContainer& algorithmContainer,const int sizePopulation,
			const int numberStepEvolution, const double desiredFitness);
	SchemePtr run( const std::string &logFitnessFileName,const std::string logStatisticsFileName);

	~Model();
};

#endif /* MODEL2_HPP_ */
