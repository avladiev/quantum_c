#include <iostream>
#include "io.hpp"

#include "model2.hpp"
#include "Logger.h"
#include "locator.hpp"
#include <unistd.h>
#include <cassert>

int main(int argc, char** argv) {

	Random::init();

	LOG_CONF("log", Logger::file_off|Logger::screen_on, DEBUG, DEBUG, 0);
	LOG(INFO, "program is running ,Random number = "<<Random::nextRandomInt(100000));

	double pMutation;
	double pInversion;
	double pMate;
	std::string baseMatrixFileName;
	std::string targetMatrixFileName;
	std::string logFitnessFileName;
	std::string logSchemeDynamicsFileName;
	int factor = 2;

	int maxSizePopulation = 100;
	int numberStepEvolution = 600;
	double desiredFitness = pow(10, -5);

	if (argc == 1) {
		pMutation = 0.4;
		pInversion = 0.4;
		pMate = 0.9;
		baseMatrixFileName = "inputData/artificialData/base_matrix.h5";
		targetMatrixFileName = "inputData/artificialData/targetMatrix1.h5";
		logFitnessFileName = "logFitness.csv";
		logSchemeDynamicsFileName = "logStatictics.csv";
	} else {
		pMutation = atof(argv[1]);
		pInversion = atof(argv[2]);
		pMate = atof(argv[3]);
		baseMatrixFileName = argv[4];
		targetMatrixFileName = argv[5];
		logFitnessFileName = argv[6];
		logSchemeDynamicsFileName = argv[7];
	}


	LOG(INFO, "input target matrix file name "<<targetMatrixFileName);
	LOG(INFO, "input base matrix file name "<<baseMatrixFileName);

	std::vector<IMatrix*>* baseMatrices = IOModule::readBaseMatrices(
			baseMatrixFileName);
	IMatrix * targetMatrix = IOModule::readTargetMatrix(targetMatrixFileName);

	LOG(INFO, "input target matrix\n" << *targetMatrix);

	//LOG(INFO, "input base matrices \n"<<Util::matrixToStr(baseMatrices));

	LOG(INFO,
			"pMutation = " <<pMutation<<",pInversion="<<pInversion<<",pMate = "<<pMate);
	LOG(INFO,
			"logFitnessFileName = "<<logFitnessFileName<<",logSchemeDynamicsFileName="<<logSchemeDynamicsFileName)

	LOG(INFO,"maxSizePopulation = "<<maxSizePopulation<<", numberStepEvolution = "<<
			numberStepEvolution<<",desiredFitness="<<desiredFitness );

	Locator::instance().init(baseMatrices, targetMatrix);

	MateImpl mate(pMate, factor);
	MutationImpl mutation(pMutation, pInversion);
	FitnessByL2Norm fitness;


	//FitnessByInfNorm fitness;
	//FitnessByOperatorL1Norm fitness;
	//FitnessByOperatorL2Norm fitness;

	ElitistTourneySelection selection(0.8);
	LOG(INFO," mate is "<<mate.toString()<<", mutation is "<<
			mutation.toString()<<", fitness is "<<fitness.toString()<<", selection is "<<selection.toString());
	AlgorithmContainer algoContainer(mate, mutation, fitness, selection);
	Model model(algoContainer, maxSizePopulation, numberStepEvolution, desiredFitness);
	SchemePtr bestScheme = model.run(logFitnessFileName,
			logSchemeDynamicsFileName);

	LOG(INFO,
			"out Scheme\n"<<bestScheme->toString()<<"\n"<<bestScheme->getSchemeMatrix());
	std::string outFile = "out.h5";

	//IOModule::write(outFile, od);

	LOG(INFO, "program is finishing");
}

