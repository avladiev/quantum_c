

#ifndef RANDOM_HPP_
#define RANDOM_HPP_

class Random{
public:
	static void init();
	static int nextRandomInt(const int maxNumber);
	static double nextRandomDouble();
};

#endif /* RANDOM_HPP_ */
