CC := h5c++ -std=c++11
CFLAGS :=-c   -O3
LDFLAGS :=
OUT_DIR:= bin/
SRC_DIR:= src/
SOURCE := io.cpp util.cpp Logger.cpp locator.cpp genetic_algorithms.cpp model2.cpp random.cpp matrix.cpp
OBJECTS := $(SOURCE:.cpp=.o)
POBJ:= $(addprefix $(OUT_DIR),$(OBJECTS))
PSOURCE :=$(addprefix $(SRC_DIR),$(SOURCE))

all: main
	
main: $(POBJ)
	$(CC)  $(POBJ) $(SRC_DIR)main.cpp -o $(OUT_DIR)$@

main2: $(POBJ)
	$(CC)  $(POBJ) $(SRC_DIR)main.cpp -o $(OUT_DIR)$@

test:$(POBJ)
	$(CC) $(POBJ) $(SRC_DIR)test.cpp -o $(OUT_DIR)$@
	
scheme_generator:$(POBJ)
	$(CC) $(POBJ) $(SRC_DIR)scheme_generator.cpp -o $(OUT_DIR)$@

scheme_generator1_2:$(POBJ)
	$(CC) $(POBJ) $(SRC_DIR)scheme_generator.cpp -o $(OUT_DIR)$@
scheme_generator1_3:$(POBJ)
	$(CC) $(POBJ) $(SRC_DIR)scheme_generator.cpp -o $(OUT_DIR)$@

scheme_generator2:$(POBJ)
	$(CC) $(POBJ) $(SRC_DIR)scheme_generator2.cpp -o $(OUT_DIR)$@
	
matrixTest:$(POBJ)
	$(CC) $(POBJ) $(SRC_DIR)matrixTest.cpp -o $(OUT_DIR)$@
$(OUT_DIR)%.o: $(SRC_DIR)%.cpp
	$(CC) $(CFLAGS) -c -o  $@ $<
	

clean:
	rm -f $(POBJ)  $(PTARGET)
