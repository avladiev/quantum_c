#!/usr/bin/python
from numpy.distutils.command.build_ext import build_ext

__author__ = 'mazzachuses'

import sys
import csv
import pylab
from matplotlib import mlab
from numpy import array


def readData(fileName):
    file = open(fileName, "rb")
    reader = csv.reader(file)
    data = []
    for row in reader:
        numberRow = []
        for s in row:
            num = int(s)
            numberRow.append(num)
        data.append(numberRow)
    file.close()
    return array(data)


def makeSettings():
    global yLim
    pylab.grid()
    yLim = pylab.ylim()
    pylab.ylim(yLim[0] - 1, yLim[1] + 1)


if __name__ == "__main__":
    fileName = sys.argv[1]
    #	fileName = "logFitness.csv"
    data = readData(fileName)
    oll = map(lambda x: x[0], data)
    becomeBest = map(lambda x: x[1], data)
    becomeWorse = map(lambda x: x[2], data)
    notChange = map(lambda x, y, z: x - y - z, oll, becomeBest, becomeWorse)
    dx = 1
    xMin = 0
    xMax = (len(becomeBest) - 1) * dx
    xList = mlab.frange(xMin, xMax, dx)

    pylab.subplot(2, 2, 1)
    pylab.plot(xList, data[:, 0], "m")
    pylab.plot(xList, data[:, 1], "g")
    pylab.plot(xList, data[:, 2], "r")
    pylab.plot(xList, data[:, 0] - data[:, 1] - data[:, 2], "b")
    pylab.title("total")
    makeSettings()

    pylab.subplot(2, 2, 2)
    pylab.plot(xList, data[:,3], "m")
    pylab.plot(xList, data[:,4], "g")
    pylab.plot(xList, data[:,5], "r")
    pylab.plot(xList, data[:, 3] - data[:, 4] - data[:, 5], "b")
    pylab.title("mutate")
    makeSettings()

    pylab.subplot(2, 2, 3)
    pylab.plot(xList, data[:,6], "m")
    pylab.plot(xList, data[:,7], "g")
    pylab.plot(xList, data[:,8], "r")
    pylab.plot(xList, data[:, 6] - data[:, 7] - data[:, 8], "b")
    pylab.title("inversion")
    makeSettings()


    pylab.subplot(2, 2, 4)
    pylab.plot(xList, data[:,9], "m")
    pylab.plot(xList, data[:,10], "g")
    pylab.plot(xList, data[:,11], "r")
    pylab.plot(xList, data[:, 9] - data[:, 10] - data[:, 11], "b")
    pylab.title("mate")
    makeSettings()




    pylab.show()

