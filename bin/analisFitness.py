# coding=utf-8
__author__ = 'mazzachuses'
import sys
import os
import csv


def extractNumberLines(baseDir):
    data = {}
    for item in os.listdir(baseDir):
        itemPath = os.path.join(baseDir, item)
        if item.startswith("logFitness"):
            size = len(list(csv.reader(open(itemPath))))
            key = int(item.replace("logFitness","").replace(".csv",""))
            data[key] = size
    res = range(0,len(data))
    for key in data:
        res[key] = data[key]
    return res


def composeRow(data,itemPath):
    pMutate = float(data[1])
    pInversion = float(data[2])
    pMate = float(data[3])
    row = []
    row.append(pMutate)
    row.append(pInversion)
    row.append(pMate)
    numbers = extractNumberLines(itemPath)
    row.extend(numbers)
    row.append(min(numbers))
    row.append(sum(numbers) / float(len(numbers)))
    row.append(max(numbers))
    return row


def main():
    baseDir = sys.argv[1]
    firstRow = ["pMutation","pInversion","pMate"]
    firstRow.extend(range(0,10,1))
    firstRow.extend(["best","middle","bad"])
    file  = open("report.csv", "wb")
    writer = csv.writer(file)
    writer.writerow(firstRow)
    for item in os.listdir(baseDir):
        itemPath = os.path.join(baseDir, item)
        if os.path.isdir(itemPath):
            tmp = item.split("_")
            if tmp[0] == "out":
                row = composeRow(tmp,itemPath)
                writer.writerow(row)


if __name__ == "__main__":
    main()