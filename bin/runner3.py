#!/usr/bin/python
__author__ = 'mazzachuses'

import subprocess
import sys
import os
import tools
import analysis
import time
import random

baseMatrixFileName = "/home/mazzachuses/work_spase/quantum_c/data/fromBooks/baseMatrix6.h5"
targetMatrixFileName = "/home/mazzachuses/work_spase/quantum_c/data/fromBooks/targetMatrix5.h5"
dirX = "./data/fromBooks/resultForMatrix5/elitistTourneySelection80/fromBaseMatrix6"

CMD = "/home/mazzachuses/work_spase/quantum_c/bin/main2 %f %f %f %s %s %s %s"


def run(targetDirName,pMutate, pInversion, pMate):
    outDirName = "out_%d_%d_%d" % (pMutate * 100, pInversion * 100, pMate * 100)
    if   outDirName not in os.listdir(targetDirName):
        outDirName = os.path.join(targetDirName,outDirName)
        os.makedirs(outDirName)
        print "create dir " + outDirName
        os.makedirs(os.path.join(outDirName,tools.LOG_DIR))
        os.makedirs(os.path.join(outDirName,tools.LOG_FITNESS_DIR))
        os.makedirs(os.path.join(outDirName,tools.LOG_SCHEME_DYNAMICS_DIR))
        for i in xrange(1, 11):
            cmd = CMD % (
                pMutate, pInversion, pMate, baseMatrixFileName, targetMatrixFileName,
                os.path.join(outDirName, tools.LOG_FITNESS_DIR, str(i) + ".csv"), os.path.join(outDirName, tools.LOG_SCHEME_DYNAMICS_DIR,
                                                                                               str(i) + ".csv") )
            print cmd
            file = open( os.path.join(outDirName, tools.LOG_DIR,  str(i) + ".txt"), "wb")
            subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE, stdout=file, stderr=file).wait()
            time.sleep(random.randint(1,30)/5.0)

            file.close()

            print "done iteration i = " + str(i) + " for "+outDirName+"\n"
        subprocess.Popen("notify-send --expire-time=10000 'done for  " + outDirName + "'", shell=True).wait()
        analysis.makeLocalAnalysis(outDirName)
    else:
        print outDirName + " just exist"



def main(outDir):
    outDir = os.path.abspath(outDir)
    for pMutate in [0.4]:
        for pInversion in [0.3]:#in [0.2, 0.3, 0.4]:
            for pMate in [0.9]:
                run(outDir,pMutate, pInversion, pMate)



if __name__ == "__main__":
    main(dirX)
