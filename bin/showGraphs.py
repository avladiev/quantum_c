#!/usr/bin/python
from numpy.distutils.command.build_ext import build_ext

__author__ = 'mazzachuses'

import sys
import pylab
import os
import tools


def interactive(data):
    numRow = 0
    while True:
        gist = tools.buildGist(data[numRow])
        tools.drawGist(gist, numRow, len(data))
        numRow = int(raw_input("enter num row\n"))


def main():
    outDirName = sys.argv[1]
    iteration = sys.argv[2]
    mode = (len(sys.argv) > 3)

    statisticsData = tools.readCsvData(os.path.join(outDirName, tools.LOG_SCHEME_DYNAMICS_DIR, str(iteration) + ".csv"), int)
    fitnessData = tools.readCsvData(os.path.join(outDirName, tools.LOG_FITNESS_DIR, str(iteration) + ".csv"), float)

    if (mode):
        pylab.ion()
    pylab.figure(1)
    tools.setStatisticsData(statisticsData)
    pylab.figure(2)
    tools.setFitnessData(fitnessData)

    if (mode):
        pylab.figure(3)
        interactive(fitnessData)
    else:
        pylab.show()


if __name__ == "__main__":
    main()


