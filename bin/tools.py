__author__ = 'mazzachuses'

from numpy.distutils.command.build_ext import build_ext

import csv
import pylab
from matplotlib import mlab
from numpy import array




LOG_DIR = "log"
LOG_FITNESS_DIR = "logFitness"
LOG_SCHEME_DYNAMICS_DIR = "logSchemeDynamics"
GRAPH_FITNESS_DIR = "graphFitness"
GRAPH_SCHEME_DYNAMICS = "graphSchemeDynamics"

LOCAL_REPORT_FILE_NAME = "localReport.csv"
TOTAL_REPORT_FILE_NAME = "totalReport.csv"

def readCsvData(fileName, type):
    file = open(fileName, "rb")
    reader = csv.reader(file)
    data = []
    for row in reader:
        numberRow = []
        for s in row:
            num = type(s)
            numberRow.append(num)
        data.append(numberRow)
    file.close()
    return data

def writeCsvData(fileName,data):
    file = open(fileName,"wb")
    writer = csv.writer(file)
    writer.writerows(data)

def makeSettings():
    pylab.grid()
    yLim = pylab.ylim()
    pylab.ylim(yLim[0] - 1, yLim[1] + 1)


def setStatisticsData(statisticsData):
    statisticsData = array(statisticsData)
    dx = 1
    xMin = 0
    xMax = (len(statisticsData[:, 0]) - 1) * dx
    xList = mlab.frange(xMin, xMax, dx)

    pylab.subplot(2, 2, 1)
    pylab.plot(xList, statisticsData[:, 0], "m")
    pylab.plot(xList, statisticsData[:, 1], "g")
    pylab.plot(xList, statisticsData[:, 2], "r")
    pylab.plot(xList, statisticsData[:, 0] - statisticsData[:, 1] - statisticsData[:, 2], "b")
    pylab.title("total")
    makeSettings()

    pylab.subplot(2, 2, 2)
    pylab.plot(xList, statisticsData[:, 3], "m")
    pylab.plot(xList, statisticsData[:, 4], "g")
    pylab.plot(xList, statisticsData[:, 5], "r")
    pylab.plot(xList, statisticsData[:, 3] - statisticsData[:, 4] - statisticsData[:, 5], "b")
    pylab.title("mutate")
    makeSettings()

    pylab.subplot(2, 2, 3)
    pylab.plot(xList, statisticsData[:, 6], "m")
    pylab.plot(xList, statisticsData[:, 7], "g")
    pylab.plot(xList, statisticsData[:, 8], "r")
    pylab.plot(xList, statisticsData[:, 6] - statisticsData[:, 7] - statisticsData[:, 8], "b")
    pylab.title("inversion")
    makeSettings()

    pylab.subplot(2, 2, 4)
    pylab.plot(xList, statisticsData[:, 9], "m")
    pylab.plot(xList, statisticsData[:, 10], "g")
    pylab.plot(xList, statisticsData[:, 11], "r")
    pylab.plot(xList, statisticsData[:, 9] - statisticsData[:, 10] - statisticsData[:, 11], "b")
    pylab.title("mate")
    makeSettings()


def setFitnessData(data):
    bestFitness = map(lambda x: x[0], data)
    badFitness = map(lambda x: x[-1], data)
    meanFitness = map(lambda x: sum(x) / float(len(x)), data)
    xMin = 0
    dx = 1
    xMax = (len(bestFitness) - 1) * dx
    xList = mlab.frange(xMin, xMax, dx)
    pylab.plot(xList, bestFitness, "g")
    pylab.plot(xList, badFitness, "r-")
    pylab.plot(xList, meanFitness, "b-")
    yLim = pylab.ylim()
    pylab.ylim(yLim[0], yLim[1] + 1)
    #pylab.legend(("bestFitness", "badFitness", "meanFitness"))
    pylab.grid()


def buildGist(row):
    interval = row[-1] - row[0]
    step = interval / 5
    gist = [[row[0], (row[0] + step), 0]]
    for i in xrange(1, 5):
        limit = gist[-1][1]
        gist.append([limit, limit + step, 0])
    gist[-1][1] = row[-1]
    cur = 0
    for val in row:
        while True:
            if val <= gist[cur][1] or cur == len(gist) - 1:
                gist[cur][2] += 1
                break
            else:
                cur += 1
    return gist


def drawGist(gist, curPosition, size):
    listY = map(lambda x: x[2], gist)
    listX = mlab.frange(1, len(listY))
    width = 0.4
    labels = map(lambda x: "%.2f,%.2f" % (x[0], x[1]), gist)
    pylab.clf()
    pylab.bar(listX, listY)
    pylab.xticks(listX + width, labels)
    pylab.title("%d of %d" % (curPosition, size))
    pylab.draw()

