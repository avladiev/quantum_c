#!/usr/bin/python
__author__ = 'mazzachuses'

import sys
import csv
import pylab
from matplotlib import mlab


def readData(fileName):
    file = open(fileName, "rb")
    reader = csv.reader(file)
    data = []

    for row in reader:
        numberRow = []
        for s in row:
            num = float(s)
            numberRow.append(num)
        numberRow.sort()
        data.append(numberRow)
    file.close()
    return data


def buildGist(row):
    interval = row[-1] - row[0]
    step = interval / 5
    gist = [[row[0], (row[0] + step), 0]]
    for i in xrange(1, 5):
        limit = gist[-1][1]
        gist.append([limit, limit + step, 0])
    gist[-1][1] = row[-1]
    cur = 0
    for val in row:
        while True:
            if val <= gist[cur][1] or cur == len(gist) - 1:
                gist[cur][2] += 1
                break
            else:
                cur += 1
    return gist


def drawGist(gist, curPosition, size):
    listY = map(lambda x: x[2], gist)
    listX = mlab.frange(1, len(listY))
    width = 0.4
    labels = map(lambda x: "%.2f,%.2f" % (x[0], x[1]), gist)
    pylab.clf()
    pylab.bar(listX, listY)
    pylab.xticks(listX + width, labels)
    pylab.title("%d of %d" % (curPosition, size))
    pylab.draw()


if __name__ == "__main__":
    fileName = sys.argv[1]
    #	fileName = "logFitness.csv"
    data = readData(fileName)
    bestFitness = map(lambda x: x[0], data)
    badFitness = map(lambda x: x[-1], data)
    meanFitness = map(lambda x: sum(x) / float(len(x)), data)
    dx = 1
    xMin = 0
    xMax = (len(bestFitness) - 1) * dx
    xList = mlab.frange(xMin, xMax, dx)
    pylab.ion()
    pylab.plot(xList, bestFitness, "g")
    pylab.plot(xList, badFitness, "r")
    pylab.plot(xList, meanFitness, "b")
    pylab.legend(("bestFitness", "badFitness", "meanFitness"))
    pylab.grid()


    pylab.figure(2)
    numRow = 0
    while True:
        gist = buildGist(data[numRow])
        drawGist(gist, numRow, len(data))
        numRow = int(raw_input("enter num row\n"))
