#!/usr/bin/python
# coding=utf-8
__author__ = 'mazzachuses'
import sys
import os
import pylab
import tools
import csv
from numpy import *
from matplotlib import mlab


def graphedFitness(baseDir):
    logFitnessDir = os.path.join(baseDir, tools.LOG_FITNESS_DIR)
    graphFitnessDir = os.path.join(baseDir, tools.GRAPH_FITNESS_DIR)
    if not os.path.exists(graphFitnessDir):
        os.makedirs(graphFitnessDir)

    for csvFileName in os.listdir(logFitnessDir):
        if csvFileName.endswith(".csv"):
            pngFileName = csvFileName.replace(".csv", "") + ".png"
            csvFile = os.path.join(logFitnessDir, csvFileName)
            fitnessData = tools.readCsvData(csvFile, float)
            pngFile = os.path.join(graphFitnessDir, pngFileName)
            pylab.clf()
            tools.setFitnessData(fitnessData)
            pylab.savefig(pngFile)


def graphedSchemeDynamics(baseDir):
    logStatisticsDir = os.path.join(baseDir, tools.LOG_SCHEME_DYNAMICS_DIR)
    graphedStatisticsDir = os.path.join(baseDir, tools.GRAPH_SCHEME_DYNAMICS)
    if not os.path.exists(graphedStatisticsDir):
        os.makedirs(graphedStatisticsDir)

    for csvFileName in os.listdir(logStatisticsDir):
        if csvFileName.endswith(".csv"):
            pngFileName = csvFileName.replace(".csv", "") + ".png"
            csvFile = os.path.join(logStatisticsDir, csvFileName)
            logStatisticsData = tools.readCsvData(csvFile, int)
            pngFile = os.path.join(graphedStatisticsDir, pngFileName)
            pylab.clf()
            tools.setStatisticsData(logStatisticsData)
            pylab.savefig(pngFile)


def cmpX(x, y):
    if float(x.replace(".csv", "")) < float(y.replace(".csv", "")):
        return -1
    elif float(x.replace(".csv", "")) > float(y.replace(".csv", "")):
        return 1
    else:
        return 0


def makeReport(baseDir):
    logFitnessDir = os.path.join(baseDir, tools.LOG_FITNESS_DIR)
    nameFile2NumRows = {}
    for csvFileName in os.listdir(logFitnessDir):
        if csvFileName.endswith(".csv"):
            csvFile = os.path.join(logFitnessDir, csvFileName)
            data = tools.readCsvData(csvFile, str)
            numberRows = len(data)
            nameFile2NumRows[csvFileName] = numberRows

    row1 = []
    for name in nameFile2NumRows:
        row1.append(name)
    row1.sort(cmp=cmpX)
    file = open(os.path.join(baseDir, tools.LOCAL_REPORT_FILE_NAME), "wb")
    writer = csv.writer(file)

    row2 = []

    for key in row1:
        row2.append(nameFile2NumRows[key])
    row1.insert(0, "fileName")
    row1.insert(1, "best")
    row1.insert(2, "mean")
    row1.insert(3, "bad")
    best = min(row2)
    mean = float(sum(row2)) / float(len(row2))
    bad = max(row2)

    row2.insert(0, os.path.basename(baseDir))
    row2.insert(1, best)
    row2.insert(2, mean)
    row2.insert(3, bad)
    writer.writerow(row1)
    writer.writerow(row2)


def makeLocalAnalysis(baseDir):
    baseDir = os.path.abspath(baseDir)
    graphedSchemeDynamics(baseDir)
    graphedFitness(baseDir)
    makeReport(baseDir)


def tryExtractData(fileName, totalData):
    if fileName.endswith(tools.LOCAL_REPORT_FILE_NAME):
        localData = tools.readCsvData(fileName, str)
        localData[1][0] = fileName
        totalData.append(localData[1])

    return totalData


def collectReport(dir, totalData):
    for fileName in os.listdir(dir):
        fileName = os.path.join(dir, fileName)
        #print fileName
        if os.path.isdir(fileName):
            collectReport(fileName, totalData)
        elif os.path.isfile(fileName):
            tryExtractData(fileName, totalData)
    return totalData


def makeTotalAnalysis(analyzedDir):
    firstRow = ["fileName", "best", "mean", "bad"]
    for i in xrange(1, 2):
        firstRow.append(i)
    data = [firstRow]
    data = collectReport(analyzedDir, data)
    rows = 1001#len(data)
    columns= len(data[0])
    numericalData = empty((rows-1,columns-1))
    for i in xrange(0,rows):
        if i != 0:
            for j in xrange(0,columns):
                if j != 0:
                    numericalData[i-1,j-1] = float(data[i][j])

    gist =  zeros(11,dtype=int32)# количество не найденых на первом, втором .. запуске
    step  = 5
    maxStep = 600
    gist2 = zeros(maxStep/step + 2,dtype=int32)# плотоность распределения

    xxx = 0
    for row in   numericalData[:,3:]:
        num = 0

        for el in row:
            if el == maxStep:
                num+=1
            if el ==50:
                xxx+=1
            for s in xrange(1,len(gist2)):
                if el<s*step:
                    gist2[s]+=1
                    break
        gist[num]+=1

    print "gist" + str(gist)
    print "sum gist "+ str(gist.sum())
    numberBed = 0
    for i in xrange(1,len(gist)):
        numberBed+=i*gist[i]
    print "number bed=" + str(numberBed)
    print "-----"
    print "step = "+str(step)
    print "gist2" + str(gist2)
    print "sum gist2 " +str(gist2.sum())
    print "xx = " + str(xxx)



    listY = gist2[1:]
    listX = mlab.frange(step, (len(listY))*step,step)
    print listY
    print listX
    print len(listX)
    print len(listY)
    width = 0.5
    pylab.bar(listX,listY)
    labels = map(lambda x:str(x),listX)
    pylab.xticks(listX + width, labels)
    pylab.show()
    data.append(gist)
    data.append(["number bed",numberBed])
    tools.writeCsvData(os.path.join(dir, tools.TOTAL_REPORT_FILE_NAME), data)


if __name__ == "__main__":
    #dir = sys.argv[1]
    #dir = "/home/mazzachuses/work_spase/quantum_c/data/artificialData/4qubits/mechanically"
    dir = "/media/mazzachuses/0E6807A168078725/doc/ifmo/диплом/data/artificialData/2qubits/mechanically5"
    print dir
    analyzedDir = os.path.abspath(dir)
    makeTotalAnalysis(analyzedDir)






